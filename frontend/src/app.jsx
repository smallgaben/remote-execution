import React, { Component } from 'react';
import { Provider } from 'react-redux';
import {
  ConnectedRouter as Router,
	routerMiddleware
} from 'react-router-redux';
import { Link } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';

import configureRoutes from './routes';
import configureStore from './redux/configureStore';
import { getLangs } from './redux/actions/langs';
import { getCommands } from './redux/actions/commands';
import { getExecutions } from './redux/actions/executions';
import Remex from './components/Remex';
import UserMenu from './components/UserMenu';
import './styles/index.scss';

const history = createHistory();
const rMiddleware = routerMiddleware(history);
const store = configureStore([rMiddleware]);

class App extends Component {
	componentWillMount() {
		this.initApp();
	}

	initApp() {
		// Refresh token here
		store.dispatch(getLangs());
		store.dispatch(getCommands());
		store.dispatch(getExecutions());
	}

  render() {
    return (
      <Provider store={store}>
				<Router history={history}>
					<div className="app">
						<Link to="/" className="app__link">
							<Remex classList="app__logo" />
						</Link>
						<UserMenu className="app__user"/>
						{configureRoutes()}
					</div>
				</Router>
			</Provider>
    )
  }
}

export default App;
