import React from 'react';
import {
  BrowserRouter as Router,
	Route,
	Redirect,
	Link,
	Switch
} from 'react-router-dom';

import Home from 'views/Home';
import SignIn from 'views/SignIn';
import SignUp from 'views/SignUp';
import Dashboard from 'views/Dashboard';
import Commands from 'views/Commands';
import AddCommand from 'views/AddCommand';
import EditCommand from 'views/EditCommand';
import Executions from 'views/Executions';
import Execution from 'views/Execution';
import AddExecution from 'views/AddExecution';
import Auth from 'views/Auth';

const NoMatch = () => (
	<div>
		Ooops... No match
	</div>
);

const routes = [
	{
		path: '/',
		component: Home,
		exact: true
	},
	{
		path: '/auth',
		component: Auth,
		routes: [
			{
				path: '/auth/signin',
				component: SignIn,
				authRedirect: '/dashboard'
			},
			{
				path: '/auth/signup',
				component: SignUp
			}
		]
	},
  {
		path: '/dashboard',
		component: Dashboard,
		auth: true,
		routes: [
			{
				path: '/dashboard/commands',
				component: Commands
			},
			{
				path: '/dashboard/executions',
				component: Executions
			},
			{
				path: '/dashboard/add-command',
				component: AddCommand
			},
			{
				path: '/dashboard/edit-command/:id',
				component: EditCommand
			},
			{
				path: '/dashboard/add-execution',
				component: AddExecution
			},
			{
				path: '/dashboard/execution/:id',
				component: Execution
			},
		]
	}
];

export const AuthenticatedRoute = ({ component: Component, routes, ...rest }) => {
	const session = !!localStorage.jwt;

	return <Route {...rest}
		render={props => (
			session ? (
				<Component {...props} routes={routes} />
			) : (
				<Redirect to={{
					pathname: '/auth/signin',
					state: { from: props.location }
				}}/>
			)
		)}
	/>
}

export const RouteWithSubRoutes = (route) => {
	const session = !!localStorage.jwt;

	if (route.auth) {
		return (
			<AuthenticatedRoute
				component={route.component}
				path={route.path}
				routes={route.routes}
			></AuthenticatedRoute>
		);
	}

	if (session && route.authRedirect) {
		return (
			<Redirect to={route.authRedirect}/>
		);
	}

	return (
		<Route
			path={route.path}
			render={props => (
				<route.component {...props} routes={route.routes}/>
			)}
		/>
	);
}

const configureRoutes = () => (
	<Switch>
		{routes.map((route, i) => (
			<RouteWithSubRoutes key={i} {...route}/>
		))}
		<Route component={Home} />
	</Switch>
);

export default configureRoutes;
