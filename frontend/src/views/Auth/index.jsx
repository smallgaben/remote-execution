import React from 'react';
import {
	Switch,
	Redirect
} from 'react-router-dom';
import { RouteWithSubRoutes } from '../../routes';
import './style.scss';

const Auth = ({ routes }) => (
	<div className="page v-auth">
		<div className="v-auth__container container">
			<div className="v-auth__content">
				<Switch>
					{routes.map((route, i) => (
						<RouteWithSubRoutes key={i} {...route}/>
					))}
					<Redirect to="/auth/signin"/>
				</Switch>
			</div>
		</div>
	</div>
);

export default Auth;
