import React from 'react';
import SignInForm from 'components/SignInForm';

const SignIn = () => (
	<div className="v-signin">
		<SignInForm />
	</div>
);

export default SignIn;
