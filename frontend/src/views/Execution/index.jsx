import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import BackIcon from 'material-ui-icons/ChevronLeft';

import Dashbar from 'components/Dashbar';
import ExecutionInfo from 'containers/ExecutionInfo';
import './style.scss';

class Execution extends Component {
	render() {
		const { id } = this.props.match.params;

		return (
			<div className="v-execution">
				<div className="v-execution__container container">
					<Dashbar
						title="Execution info"
						button={
							<Link to="/dashboard/executions">
								<BackIcon />
							</Link>
						}
					/>
					<ExecutionInfo id={id} />
				</div>
			</div>
		);
	}
}

export default Execution;
