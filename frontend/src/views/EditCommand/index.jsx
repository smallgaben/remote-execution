import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import BackIcon from 'material-ui-icons/ChevronLeft';

import EditCommandForm from 'components/EditCommandForm';
import Dashbar from 'components/Dashbar';
import 'styles/command.scss';

class EditCommand extends Component {
	render() {
		const { id } = this.props.match.params;

		return (
			<div className="v-command">
				<div className="v-command__container container">
					<Dashbar
						title="Edit command"
						button={
							<Link to="/dashboard/commands">
								<BackIcon />
							</Link>
						}
					/>
					<EditCommandForm id={id}/>
				</div>
			</div>
		);
	}
}

export default EditCommand;
