import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { RouteWithSubRoutes } from 'routes';
import DashDrawer from 'components/DashDrawer';
import './style.scss';

class Dashboard extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { routes, location } = this.props;
		const shouldRedirect = /\/dashboard\/?$/.test(location.pathname);

		return shouldRedirect ? (
			<Redirect to="/dashboard/commands"/>
		) : (
			<div className="page v-dashboard">
				<div className="v-dashboard__body">
					<DashDrawer location={location}/>
					<main className="v-dashboard__main">
						{routes.map((route, i) => (
							<RouteWithSubRoutes key={i} {...route}/>
						))}
					</main>
				</div>
			</div>
		);
	}
}

export default Dashboard;
