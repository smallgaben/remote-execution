import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import BackIcon from 'material-ui-icons/ChevronLeft';

import AddCommandForm from 'components/AddCommandForm';
import Dashbar from 'components/Dashbar';
import 'styles/command.scss';

class AddCommand extends Component {
	render() {
		return (
			<div className="v-command">
				<div className="v-command__container container">
					<Dashbar
						title="Add command"
						button={
							<Link to="/dashboard/commands">
								<BackIcon />
							</Link>
						}
					/>
					<AddCommandForm />
				</div>
			</div>
		);
	}
}

export default AddCommand;
