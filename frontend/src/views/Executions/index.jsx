import React from 'react';
import { Link } from 'react-router-dom';
import AddIcon from 'material-ui-icons/Add';
import Typography from 'material-ui/Typography';

import Dashbar from 'components/Dashbar';
import ExecutionsTable from 'containers/ExecutionsTable';

const Executions = () => (
	<div className="v-executions">
		<div className="container">
			<Dashbar
				title="Executions"
				button={
					<Link to="/dashboard/add-execution">
						<AddIcon />
					</Link>
				}
			/>
			<ExecutionsTable />
		</div>
	</div>
);

export default Executions;
