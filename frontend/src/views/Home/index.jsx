import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

import './style.scss';
import RemexStages from 'components/RemexStages';
import Technologies from 'components/Technologies';

class Home extends Component {
	render() {
		return (
			<div className="page v-home">
				<div className="v-home__content">
					<div className="v-home__container container">
						<h1 className="v-home__title">
							Remote <span>Execution</span>
						</h1>
						<div className="v-home__description">
							Run code without thinking about servers.
							<br />
							Pay for only the compute time you consume.
						</div>
						<RemexStages />
						<Technologies />
						<div className="v-home__call">
							<Link className="v-home__call-link" to="/auth/signin">
								<Button type="submit" className="v-home__call-button" raised color="accent">
									Take all my money, i wanna join right now!
								</Button>
							</Link>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Home;
