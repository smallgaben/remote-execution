import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import BackIcon from 'material-ui-icons/ChevronLeft';

import AddExecutionForm from 'components/AddExecutionForm';
import Dashbar from 'components/Dashbar';
import 'styles/command.scss';

class AddExecution extends Component {
	render() {
		return (
			<div className="v-execution">
				<div className="v-execution__container container">
					<Dashbar
						title="Add execution"
						button={
							<Link to="/dashboard/executions">
								<BackIcon />
							</Link>
						}
					/>
					<AddExecutionForm />
				</div>
			</div>
		);
	}
}

export default AddExecution;
