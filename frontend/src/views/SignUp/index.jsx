import React from 'react';
import SignUpForm from 'components/SignUpForm';

const SignUp = () => (
	<div className="v-signup">
		<SignUpForm />
	</div>
);

export default SignUp;
