import React from 'react';
import { Link } from 'react-router-dom';
import AddIcon from 'material-ui-icons/Add';
import Typography from 'material-ui/Typography';

import Dashbar from 'components/Dashbar';
import CommandsTable from 'containers/CommandsTable';
import './style.scss';

const Commands = () => (
	<div className="v-commands">
		<div className="container">
			<Dashbar
				title="Commands"
				button={
					<Link to="/dashboard/add-command">
						<AddIcon />
					</Link>
				}
			/>
			<CommandsTable />
		</div>
	</div>
);

export default Commands;
