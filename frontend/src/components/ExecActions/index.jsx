import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PlayIcon from 'material-ui-icons/PlayArrow';
import PauseIcon from 'material-ui-icons/Pause';
import StopIcon from 'material-ui-icons/Stop';
import EditIcon from 'material-ui-icons/Settings';

import './style.scss';

class ExecActions extends Component {
	render() {
		const { link } = this.props;

		return (
			<div className="exec-actions">
				{/* <div className="exec-actions__button">
					<PlayIcon />
				</div> */}
				{/* <div className="exec-actions__button">
					<PauseIcon />
				</div> */}
				{/* <div className="exec-actions__button">
					<StopIcon />
				</div> */}
				{link ? (
					<Link to={link} className="exec-actions__button exec-actions__link">
						<EditIcon />
					</Link>
				) : null}
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		prop: state.prop
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		dispatch1: () => {
			dispatch(actionCreator)
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ExecActions);
