import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ValidatorForm } from 'react-material-ui-form-validator';

import './style.scss';

class Form extends Component {
	componentWillMount() {
		const { formData } = this.props;

		ValidatorForm.addValidationRule('minLength', (value, length) => {
			if (value.length < length) {
				return false;
			}
			return true;
		});

		ValidatorForm.addValidationRule('maxLength', (value, length) => {
			if (value.length > length) {
				return false;
			}
			return true;
		});

		ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
			if (value !== formData.password) {
				return false;
			}
			return true;
		});
	}

	render() {
		const { children, ...restProps } = this.props;

		return (
			<ValidatorForm
				className="form"
				{...restProps}
			>
				<div className="form__body">
					{children}
				</div>
			</ValidatorForm>
		);
	}
}

Form.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.element),
		PropTypes.element,
	])
};

export default Form;
