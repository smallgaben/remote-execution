import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Done from 'material-ui-icons/Done';
import { withStyles } from 'material-ui/styles';
import { FormControl, FormHelperText } from 'material-ui/Form';
import { TextValidator } from 'react-material-ui-form-validator';

import Actions from 'redux/actions/auth';
import Form from 'components/Form';
import SignUpSuccess from 'components/SignUpSuccess';

const styles = theme => ({
  rightIcon: {
    marginLeft: theme.spacing.unit,
	},
	formError: {
		margin: '0 0 10px'
	}
});

class SignUpForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			form: {
				username: '',
				email: '',
				password: '',
				passwordConfirm: ''
			}
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentWillMount() {
		const { signUpReset } = this.props;

		signUpReset();
	}

	handleChange(e) {
		const { form } = this.state;

		form[e.target.name] = e.target.value;
		this.setState({ form });
	}

	handleSubmit(e) {
		e.preventDefault();

		const { signUp } = this.props;
		const { username, password, email } = this.state.form;

		signUp({
			username,
			password,
			email
		});
	}

	_renderForm() {
		const {
			classes,
			error,
			success
		} = this.props;
		const { form } = this.state;

		return success ? (
			<div>
				<FormControl fullWidth className="form__row">
					<SignUpSuccess />
				</FormControl>
			</div>
		) : (
			<div>
				<Typography type="title" className="form__title">
					Sign Up
				</Typography>
				<FormControl fullWidth className="form__row">
					<TextValidator
						label="Username"
						name="username"
						className="form__input"
						margin="normal"
						autoComplete="off"
						value={form.username}
						onChange={this.handleChange}
						validators={['required', 'minLength:3']}
						errorMessages={['This field is required', 'Min length - 3']}
					/>
				</FormControl>
				<FormControl fullWidth className="form__row">
					<TextValidator
						label="Email"
						name="email"
						type="email"
						className="form__input"
						margin="normal"
						autoComplete="off"
						value={form.email}
						onChange={this.handleChange}
						validators={['required', 'isEmail']}
						errorMessages={['This field is required', 'Wrong email format']}
					/>
				</FormControl>
				<FormControl fullWidth className="form__row">
					<TextValidator
						label="Password"
						name="password"
						className="form__input"
						margin="normal"
						type="password"
						autoComplete="off"
						value={form.password}
						onChange={this.handleChange}
						validators={['required', 'minLength:4']}
						errorMessages={['This field is required', 'Min length - 4']}
					/>
				</FormControl>
				<FormControl fullWidth className="form__row">
					<TextValidator
						label="Confirm password"
						name="passwordConfirm"
						className="form__input"
						margin="normal"
						type="password"
						autoComplete="off"
						value={form.passwordConfirm}
						onChange={this.handleChange}
						validators={['isPasswordMatch', 'required']}
						errorMessages={['Password mismatch', 'This field is required']}
					/>
					<FormHelperText className={classes.formError + ' form__error'}>
						{error}
					</FormHelperText>
				</FormControl>
				<FormControl className="form__row form__row--submit">
					<Link to="/auth/signin">
						<Typography type="button">
							Sign in
						</Typography>
					</Link>
					<Button type="submit" raised color="primary">
						Sign Up
						<Done className={classes.rightIcon} />
					</Button>
				</FormControl>
			</div>
		);
	}

	render() {
		const { form } = this.state;

		return (
			<Form
				ref="form"
				onSubmit={this.handleSubmit}
				formData={form}
			>
				{this._renderForm()}
			</Form>
		);
	}
}

SignUpForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ signup }, ownProps) => {
	return {
		pending: signup.pending,
		error: signup.error,
		success: signup.success
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		signUpReset: () => {
			dispatch(Actions.signUpReset());
		},
		signUp: (credentials) => {
			dispatch(Actions.signUp(credentials));
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SignUpForm));
