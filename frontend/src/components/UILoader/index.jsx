import React from 'react';

import Remex from 'components/Remex';
import './style.scss';

export const UILoader = (props) => {
	const { className } = props;

	return (
		<div className={`ui-loader ${className}`}>
			<Remex />
		</div>
	)
}

export default UILoader;
