import React, { Component } from 'react';
import AceEditor from 'react-ace';
import Typography from 'material-ui/Typography';
import Select from 'material-ui/Select';
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import 'brace/mode/jsx';
import './style.scss';

const modes = [
  'javascript',
	'java',
  'python',
  'ruby',
]

const themes = [
  'monokai',
  'github',
]

modes.forEach((lang) => {
  require(`brace/mode/${lang}`)
  require(`brace/snippets/${lang}`)
})

themes.forEach((theme) => {
  require(`brace/theme/${theme}`)
})

import 'brace/ext/language_tools';
import 'brace/ext/searchbox';

const defaultValue = '';
const defaultMode = 'javascript';

class Editor extends Component {
  constructor(props) {
		super(props);

    this.state = {
      value: props.value || defaultValue,
      theme: 'monokai',
      mode: this.getNormalizedMode(props.selectedLang) || defaultMode,
      enableBasicAutocompletion: true,
      enableLiveAutocompletion: false,
      fontSize: 14,
      showGutter: true,
      showPrintMargin: false,
      highlightActiveLine: true,
      enableSnippets: false,
			showLineNumbers: true,
			error: ''
    };

    this.setTheme = this.setTheme.bind(this);
    this.setMode = this.setMode.bind(this);
    this.onChange = this.onChange.bind(this);
    this.setFontSize = this.setFontSize.bind(this);
    this.setBoolean = this.setBoolean.bind(this);
    this.onValidate = this.onValidate.bind(this);
	}

	componentWillReceiveProps({ value, selectedLang }) {
		let newState = {};

		if (value) {
			newState.value = value;
		}

		if (selectedLang) {
			newState.mode = this.getNormalizedMode(selectedLang) || defaultMode;
		}

		this.setState(newState);
	}

  onChange(newValue) {
		const { handleCustomChange } = this.props;

    this.setState({
      value: newValue
		});

		if (handleCustomChange) {
			handleCustomChange(newValue);
		}
  }

  onValidate(annotations) {
		const { handleCustomError } = this.props;
		let hasError = false;

		if (annotations && annotations.length) {
			this.setState({
				error: 'There are some errors in code.'
			});
			hasError = true;
		} else {
			this.setState({
				error: ''
			});
		}

		if (handleCustomError) {
			handleCustomError(hasError);
		}
  }

  setTheme(e) {
    this.setState({
      theme: e.target.value
    })
	}

  setMode(e) {
    this.setState({
      mode: e.target.value
    })
	}

  setBoolean(name, value) {
    this.setState({
      [name]: value
    })
	}

  setFontSize(e) {
    this.setState({
      fontSize: parseInt(e.target.value, 10)
    })
	}

	getNormalizedMode(lang = '') {
		const modeIndex = modes.indexOf(lang.replace(/_/g, '').toLowerCase());

		if (modeIndex === -1) {
			return false;
		}

		return modes[modeIndex];
	}

  render() {
		const { empty, readOnly } = this.props;
		let error = this.state.error;

		if (!error && empty) {
			error = 'Empty code field';
		}

    return (
      <div className="editor">
				<div className="editor__ui">
					<FormControl className="editor__group">
						<InputLabel>Syntax</InputLabel>
						<Select
							native
							value={this.state.mode}
							onChange={this.setMode}
							name="mode"
							className="editor__select"
							input={<Input />}
						>
							{modes.map((lang) => <option  key={lang} value={lang}>{lang}</option>)}
						</Select>
          </FormControl>
          <FormControl className="editor__group">
						<InputLabel>Theme</InputLabel>
						<Select
							native
							value={this.state.theme}
							onChange={this.setTheme}
							name="theme"
							className="editor__select"
							input={<Input />}
						>
							{themes.map((lang) => <option key={lang} value={lang}>{lang}</option>)}
						</Select>
          </FormControl>
				</div>
        <FormControl className="editor__content">
					<InputLabel shrink={true}>Code</InputLabel>
					<AceEditor
						ref="editor"
						mode={this.state.mode}
						theme={this.state.theme}
						name="remex-editor"
						readOnly={readOnly}
						onChange={this.onChange}
						onValidate={this.onValidate}
						value={this.state.value}
						height="400px"
						width="100%"
						fontSize={this.state.fontSize}
						showPrintMargin={this.state.showPrintMargin}
						showGutter={this.state.showGutter}
						highlightActiveLine={this.state.highlightActiveLine}
						className="editor__body"
						setOptions={{
							enableBasicAutocompletion: this.state.enableBasicAutocompletion,
							enableLiveAutocompletion: this.state.enableLiveAutocompletion,
							enableSnippets: this.state.enableSnippets,
							showLineNumbers: this.state.showLineNumbers,
							tabSize: 2,
						}}
					/>
					<FormHelperText error={true} className="editor__error">{error}</FormHelperText>
        </FormControl>
      </div>
    );
  }
}

export default Editor;
