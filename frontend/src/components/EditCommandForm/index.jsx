import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import { FormControl, FormHelperText } from 'material-ui/Form';
import { TextValidator } from 'react-material-ui-form-validator';
import AddIcon from 'material-ui-icons/Add';
import BackIcon from 'material-ui-icons/ChevronLeft';
import Form from 'components/Form';
import Editor from 'components/Editor';

import Actions from 'redux/actions/commands';
import { getCommandById } from 'redux/reducers/commands';

const styles = theme => ({
  rightIcon: {
    marginLeft: theme.spacing.unit,
	},
	formError: {
		margin: '0 0 10px'
	}
});

class EditCommandForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			form: {
				name: '',
				description: '',
				code: '',
				lang: 'JAVA_SCRIPT'
			}
		};

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentWillMount() {
		this.setState({
			form: this.props.command
		});
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			form: nextProps.command
		});
	}

	handleSubmit(e) {
		e.preventDefault();
	}

	_renderSelectLangs() {
		const { form } = this.state;
		const { langs } = this.props;

		return (
			<Select
				native
				value={form.lang}
				readOnly={true}
				name="lang"
				className="langs"
				input={<Input />}
			>
				{langs.map(
					lang => (
            <option key={lang.value} value={lang.value}>{lang.title}</option>
					)
				)}
			</Select>
		);
	}

	_renderForm() {
		const {
			form
		} = this.state;
		const {
			classes
		} = this.props;

		return (
			<div>
				<div className="form__group">
					<FormControl fullWidth className="form__row">
						<TextValidator
							label="Name"
							name="name"
							className="form__input"
							margin="normal"
							autoComplete="off"
							readOnly={true}
							value={form.name}
							validators={['required', 'minLength:3']}
							errorMessages={['This field is required', 'Min length - 3']}
						/>
					</FormControl>
					<FormControl fullWidth className="form__row">
						<TextValidator
							label="Description"
							name="description"
							className="form__input"
							margin="normal"
							autoComplete="off"
							value={form.description}
						/>
					</FormControl>
					<FormControl fullWidth className="form__row">
						<InputLabel>Language</InputLabel>
						{this._renderSelectLangs()}
					</FormControl>
				</div>
				<FormControl fullWidth className="form__row">
					<Editor value={form.code} readOnly={true} selectedLang={form.lang}/>
				</FormControl>
				<FormControl className="form__row form__row--submit">
					<Link to="/dashboard/commands" className="form__submit">
						<Button type="submit" raised color="accent">
							Back
							<BackIcon className={classes.rightIcon} />
						</Button>
					</Link>
				</FormControl>
			</div>
		);
	}

	render() {
		const { form } = this.state;

		return (
			<Form
				ref="form"
				onSubmit={this.handleSubmit}
				formData={form}
			>
				{this._renderForm()}
			</Form>
		);
	}
}

const mapStateToProps = ({ commands, langs }, { id }) => {
	return {
		langs: langs.data,
		command: getCommandById(commands.all.data, id)
	}
};

export default connect(mapStateToProps, null)(withStyles(styles)(EditCommandForm));
