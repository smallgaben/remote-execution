import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Done from 'material-ui-icons/Done';
import { withStyles } from 'material-ui/styles';
import { FormControl, FormHelperText } from 'material-ui/Form';
import { TextValidator } from 'react-material-ui-form-validator';

import Actions from 'redux/actions/auth';
import Form from 'components/Form';

const styles = theme => ({
  button: {},
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
	},
	formError: {
		margin: '0 0 10px'
	}
});

class SignInForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			form: {
				username: '',
				password: ''
			}
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentWillMount() {
		const { signInReset } = this.props;

		signInReset();
	}

	handleChange(e) {
		const { form } = this.state;

		form[e.target.name] = e.target.value;
		this.setState({ form });
	}

	handleSubmit(e) {
		e.preventDefault();

		const { signIn } = this.props;
		const { username, password } = this.state.form;

		signIn({
			username,
			password
		});
	}

	render() {
		const {
			classes,
			pending,
			error
		} = this.props;
		const { form } = this.state;

		return (
			<Form
				ref="form"
				onSubmit={this.handleSubmit}
				formData={form}
			>
				<Typography type="title" className="form__title">
					Sign In
				</Typography>
				<FormControl fullWidth className="form__row">
					<TextValidator
						label="Username"
						className="form__input"
						margin="normal"
						name="username"
						autoComplete="off"
						value={form.username}
						onChange={this.handleChange}
						validators={['required', 'minLength:3']}
						errorMessages={['This field is required', 'Min length - 3']}
					/>
				</FormControl>
				<FormControl fullWidth className="form__row">
					<TextValidator
						label="Password"
						className="form__input"
						margin="normal"
						type="password"
						name="password"
						autoComplete="off"
						value={form.password}
						onChange={this.handleChange}
						validators={['required', 'minLength:4']}
						errorMessages={['This field is required', 'Min length - 4']}
					/>
					<FormHelperText className={classes.formError + ' form__error'}>
						{error}
					</FormHelperText>
				</FormControl>
				<FormControl fullWidth className="form__row form__row--submit">
					<Link to="/auth/signup">
						<Typography type="button">
							Sign up
						</Typography>
					</Link>
					<Button type="submit" className={classes.button} raised color="accent">
						Login
						<Done className={classes.rightIcon} />
					</Button>
				</FormControl>
			</Form>
		)
	}
}

SignInForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ auth }, ownProps) => {
	return {
		pending: auth.pending,
		error: auth.error
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		signIn: (credentials) => {
			dispatch(Actions.signIn(credentials));
		},
		signInReset: () => {
			dispatch(Actions.signInReset());
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SignInForm));
