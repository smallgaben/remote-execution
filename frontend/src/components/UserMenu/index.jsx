import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { NavLink } from 'react-router-dom';
import Button from 'material-ui/Button';
import { MenuItem, MenuList } from 'material-ui/Menu';
import Grow from 'material-ui/transitions/Grow';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import { Manager, Target, Popper } from 'react-popper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import ClickAwayListener from 'material-ui/utils/ClickAwayListener';
import AccountIcon from 'material-ui-icons/AccountCircle';
import LogoutIcon from 'material-ui-icons/PowerSettingsNew';
import DashboardIcon from 'material-ui-icons/Dashboard';

import Actions from 'redux/actions/auth';
import './style.scss';

const styles = {
  root: {
    display: 'flex',
  },
  popperClose: {
    pointerEvents: 'none',
  },
};

class UserMenu extends Component {
	constructor(props) {
		super(props);

		this.state = {
			open: false
		};
		this.handleLogout = this.handleLogout.bind(this);
	}

	handleClick = () => {
    this.setState({ open: !this.state.open });
  };

  handleClose = () => {
    this.setState({ open: false });
	};

	handleLogout() {
		const { logout } = this.props;

		logout();
	}

	render() {
		const { session, className, classes } = this.props;
		const { open } = this.state;

		return session ? (
			<div className={`${className} usermenu ` + classNames({ 'is-active': open })}>
				<Manager>
					<Target>
						<AccountIcon
							onClick={this.handleClick}
							className="usermenu__icon"
						/>
					</Target>
					<Popper
						placement="bottom-end"
						eventsEnabled={open}
						className={classNames({ [classes.popperClose]: !open })}
					>
						<ClickAwayListener onClickAway={this.handleClose}>
							<Grow in={open} style={{ transformOrigin: '0 0 0' }}>
								<Paper>
									<List role="menu" className="usermenu__list">
										<ListItem onClick={this.handleClose} className="usermenu__item">
											<NavLink activeClassName="is-active" className="usermenu__link" to="/dashboard">
												<ListItemIcon className="usermenu__item-icon">
													<DashboardIcon />
												</ListItemIcon>
												<ListItemText className="usermenu__item-text" inset primary="Dashboard" />
											</NavLink>
										</ListItem>
										<ListItem onClick={this.handleLogout} className="usermenu__item usermenu__link">
											<ListItemIcon className="usermenu__item-icon">
												<LogoutIcon />
											</ListItemIcon>
											<ListItemText className="usermenu__item-text" inset primary="Logout" />
										</ListItem>
									</List>
								</Paper>
							</Grow>
						</ClickAwayListener>
					</Popper>
				</Manager>
			</div>
		) : null;
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		logout: () => {
			dispatch(Actions.logout());
		}
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		session: state.auth.session
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserMenu));
