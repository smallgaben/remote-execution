import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Checkbox from 'material-ui/Checkbox';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Done from 'material-ui-icons/Done';
import EditIcon from 'material-ui-icons/Settings';
import AddIcon from 'material-ui-icons/Add';

import UILoader from 'components/UILoader';
import Actions from 'redux/actions/executions';
import './style.scss';

class AddExecutionForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selected: null
		};
	}

	componentWillMount() {
		const { addExecutionReset } = this.props;

		addExecutionReset();
	}

	isChecked = (id) => {
		return (this.state.selected === id);
	}

	handleCheckbox = (e, id) => {
		let selected = null;

		if (e.target.checked) {
			selected = id;
		}

		this.setState({
			selected
		});
	}

	handleSubmit = (e) => {
		e.preventDefault();

		const { addExecution } = this.props;

		addExecution(this.state.selected);
	}

	handleReset = () => {
		const { addExecutionReset } = this.props;

		this.setState({
			selected: null
		})
		addExecutionReset();
	}

	_renderSelected() {
		const { selected } = this.state;

		return (
			<div className="add-execution__selected">
				<Typography type="body2" className="add-execution__selected-typo">
					{
						selected ?
							`Selected command with id ${selected}` :
							'Pick one command to create new execution'
					}
				</Typography>
				<Button
					disabled={!selected}
					raised
					color="accent"
					onClick={this.handleSubmit}
					className="add-execution__selected-button"
				>
					Add
					<Done className="add-execution__selected-icon" />
				</Button>
			</div>
		);
	}

	_renderExecution() {
		const { commands, error, success, pending } = this.props;
		const { selected } = this.state;

		if (success) {
			return (
				<div className="add-execution__success">
					Execution has been added successfully!
					<br/>
					<AddIcon onClick={this.handleReset} />
				</div>
			);
		}

		return (
			<div>
				{this._renderSelected()}
				<div className="add-execution__commands">
					{error ? error : ''}
					<Table>
						<TableHead className="commands-table__head">
							<TableRow>
								<TableCell className="commands-table__cell">Select</TableCell>
								<TableCell className="commands-table__cell" numeric>ID</TableCell>
								<TableCell className="commands-table__cell">Name</TableCell>
								<TableCell className="commands-table__cell">Language</TableCell>
								<TableCell className="commands-table__cell">Actions</TableCell>
							</TableRow>
						</TableHead>
						<TableBody className="commands-table__body">
							{commands.map((c) => {
								const isChecked = this.isChecked(c.id);

								return (
									<TableRow className="commands-table__row" key={c.id}>
										<TableCell className="commands-table__cell">
											<Checkbox
												checked={isChecked}
												onChange={e => this.handleCheckbox(e, c.id)}
											/>
										</TableCell>
										<TableCell className="commands-table__cell" numeric>{c.id}</TableCell>
										<TableCell className="commands-table__cell">{c.name}</TableCell>
										<TableCell className="commands-table__cell">{c.lang}</TableCell>
										<TableCell className="commands-table__cell">
											<div className="commands-table__actions">
												<Link to={`/dashboard/edit-command/${c.id}`}>
													<EditIcon />
												</Link>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</div>
			</div>
		);
	}

	render() {
		const { pending, success } = this.props;
		let successClass = '';

		if (success) {
			successClass = 'add-execution--loaded';
		}

		return (
			<div className={`add-execution ${successClass}`}>
				{pending ? (
					<UILoader className="add-execution__loader"/>
				) : null}
				{this._renderExecution()}
			</div>
		);
	}
}

const mapStateToProps = ({ commands, executions }, ownProps) => {
	return {
		commands: commands.all.data,
		pending: executions.add.pending,
		error: executions.add.error,
		success: executions.add.success,
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addExecution: (commandId) => {
			dispatch(Actions.addExecution(commandId));
		},
		addExecutionReset: () => {
			dispatch(Actions.addExecutionReset());
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(AddExecutionForm);
