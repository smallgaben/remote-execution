import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink, Link } from 'react-router-dom';
import Drawer from 'material-ui/Drawer';
import Typography from 'material-ui/Typography';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import LogoutIcon from 'material-ui-icons/PowerSettingsNew';
import CommandsIcon from 'material-ui-icons/Code';
import ExecutionsIcon from 'material-ui-icons/CloudQueue';

import Actions from 'redux/actions/auth';
import './style.scss';

class DashDrawer extends Component {
	constructor(props) {
		super(props);

		this.handleLogout = this.handleLogout.bind(this);
	}

	handleLogout() {
		const { logout } = this.props;

		logout();
	}

	render() {
		return (
			<aside className="v-dashboard__drawer drawer">
				<Link className="drawer__header" to="/dashboard/commands">Dashboard</Link>
				<List className="drawer__list">
					<ListItem className="drawer__section">
						<Typography type="button">Manage</Typography>
					</ListItem>
					<ListItem className="drawer__item">
						<NavLink activeClassName="is-active" to="/dashboard/commands" className="drawer__link">
							<ListItemIcon>
								<CommandsIcon className="drawer__icon"/>
							</ListItemIcon>
							<ListItemText primary="Commands" className="drawer__item-text"/>
						</NavLink>
					</ListItem>
					<ListItem className="drawer__item">
						<NavLink activeClassName="is-active" to="/dashboard/executions" className="drawer__link">
							<ListItemIcon>
								<ExecutionsIcon className="drawer__icon"/>
							</ListItemIcon>
							<ListItemText primary="Executions" className="drawer__item-text"/>
						</NavLink>
					</ListItem>
					<ListItem className="drawer__section">
						<Typography type="button">Profile</Typography>
					</ListItem>
					<ListItem className="drawer__item">
						<div className="drawer__link" onClick={this.handleLogout}>
							<ListItemIcon>
								<LogoutIcon className="drawer__icon"/>
							</ListItemIcon>
							<ListItemText primary="Logout" className="drawer__item-text"/>
						</div>
					</ListItem>
				</List>
			</aside>
		);
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		logout: () => {
			dispatch(Actions.logout());
		}
	}
}

export default connect(null, mapDispatchToProps)(DashDrawer);
