import React from 'react';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';

import './style.scss';
import shareSVG from 'assets/images/svg/share.svg';
import computeSVG from 'assets/images/svg/compute.svg';
import discoverSVG from 'assets/images/svg/discover.svg';

const RemexStages = (props) => {
	return(
		<div className="remex-stages">
			<Grid container className="remex-stages__list">
				<Grid item xs={12} sm={4} className="remex-stage">
					<img className="remex-stage__icon" src={shareSVG} alt="share your task"/>
					<Typography className="remex-stage__label" type="button">
						Share your task
					</Typography>
				</Grid>
				<Grid item xs={12} sm={4} className="remex-stage">
					<img className="remex-stage__icon" src={computeSVG} alt="let us to compute"/>
					<Typography className="remex-stage__label" type="button">
						Let us to compute
					</Typography>
				</Grid>
				<Grid item xs={12} sm={4} className="remex-stage">
					<img className="remex-stage__icon" src={discoverSVG} alt="discover knowledge"/>
					<Typography className="remex-stage__label" type="button">
						Discover knowledge
					</Typography>
				</Grid>
			</Grid>
		</div>
	)
}

export default RemexStages;
