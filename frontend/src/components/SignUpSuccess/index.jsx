import React from 'react';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import { Link } from 'react-router-dom';

const successMessage = {
	margin: '25px 0 10px',
	textAlign: 'right'
};

export const SignUpSuccess = (props) => {
	return (
		<div style={successMessage}>
			<Typography type="button">
				You have signed up successfully!
			</Typography>
			<br />
			<Link to="/auth/signin">
				<Button type="button" raised color="accent">
					Sign In
				</Button>
			</Link>
		</div>
	);
}

export default SignUpSuccess;
