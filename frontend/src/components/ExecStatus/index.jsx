import React from 'react';
import PropTypes from 'prop-types';
import { red, yellow, green } from 'material-ui/colors';
import Tooltip from 'material-ui/Tooltip';

import { EXEC_STATUSES } from '../../config';

import './style.scss';

export const ExecStatus = (props) => {
	let { status } = props;
	let color;

	switch (status) {
		case EXEC_STATUSES.EXECUTING:
			color = green['A400'];
			break;
		case EXEC_STATUSES.STOPPED:
			color = red[500];
			break;
		case EXEC_STATUSES.PAUSED:
			color = yellow[500];
			break;
		case EXEC_STATUSES.FAILED:
			color = red[500]
			break;
		case EXEC_STATUSES.FINISHED:
			color = green['A400'];
			break;
		default:
			status = false;
			break;
	}

	let statusClass = '';

	if (status) {
		statusClass = status.toLowerCase();
	}

	return (
		<div className={`exec-status exec-status--${statusClass}`}>
			{status ? (
				<Tooltip placement="bottom" title={status}>
					<svg className="exec-status__svg" viewBox="25 25 50 50">
						<circle className="exec-status__circle" cx="50" cy="50" r="20" fill="none" stroke={color} strokeWidth="5" />
					</svg>
				</Tooltip>
			) : null}
		</div>
	);
}

ExecStatus.propTypes = {
	status: PropTypes.string
};

export default ExecStatus;
