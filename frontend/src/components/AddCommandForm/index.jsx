import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import Button from 'material-ui/Button';
import Done from 'material-ui-icons/Done';
import { withStyles } from 'material-ui/styles';
import { FormControl, FormHelperText } from 'material-ui/Form';
import { TextValidator } from 'react-material-ui-form-validator';
import AddIcon from 'material-ui-icons/Add';
import Form from 'components/Form';
import Editor from 'components/Editor';
import Actions from 'redux/actions/commands';

const styles = theme => ({
  rightIcon: {
    marginLeft: theme.spacing.unit,
	},
	formError: {
		margin: '0 0 10px'
	}
});

class AddCommandForm extends Component {
	constructor(props) {
		super(props);

		this.defaultLang = '';
		this.state = {
			form: {
				name: '',
				description: '',
				code: '',
				lang: this.defaultLang
			},
			editorError: null,
			editorEmpty: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleReset = this.handleReset.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleChangeLang = this.handleChangeLang.bind(this);
		this.handleEditorChange = this.handleEditorChange.bind(this);
		this.handleEditorError = this.handleEditorError.bind(this);
	}

	componentWillMount() {
		const { langs } = this.props;

		this.handleReset();
		this.setDefaultLang(langs);
	}

	componentWillReceiveProps(nextProps) {
		const { langs } = nextProps;

		this.setDefaultLang(langs);
	}

	setDefaultLang(langs) {
		const { form } = this.state;

		if (langs.length && this.defaultLang === '') {
			this.defaultLang = langs[0].value;
			this.setState({
				form: {
					...form,
					lang: this.defaultLang
				}
			})
		}
	}

	handleReset(e) {
		const { addCommandReset } = this.props;

		this.setState({
			form: {
				name: '',
				description: '',
				code: '',
				lang: this.defaultLang
			}
		})
		addCommandReset();
	}

	handleEditorChange(value) {
		const { form } = this.state;
		const empty = this.isCodeEmpty(value);

		this.setState({
			form: {
				...form,
				code: value
			},
			editorEmpty: empty
		});
	}

	handleEditorError(error) {
		this.setState({
			editorError: error
		});
	}

	handleChange(e) {
		const { form } = this.state;

		form[e.target.name] = e.target.value;
		this.setState({ form });
	}

	handleChangeLang(e) {
		const { form } = this.state;

		form['lang'] = e.target.value;
		this.setState({ form });
	}

	handleSubmit(e) {
		e.preventDefault();

		const { addCommand } = this.props;
		const { form } = this.state;

		if (this.isCodeEmpty()) {
			this.setState({
				editorEmpty: true
			});
			return;
		}

		if (this.state.editorError) {
			return;
		}

		addCommand(form);
	}

	isCodeEmpty(value) {
		let code = value;

		if (!code) {
			code = this.state.form.code;
		}

		return (code.replace(/\n|\s/g, '').length === 0);
	}

	_renderSelectLangs() {
		const { form } = this.state;
		const { langs } = this.props;

		return (
			<Select
				native
				value={form.lang}
				onChange={this.handleChangeLang}
				name="lang"
				className="langs"
				input={<Input />}
			>
				{langs.map(
					lang => (
            <option key={lang.value} value={lang.value}>{lang.title}</option>
					)
				)}
			</Select>
		);
	}

	_renderForm() {
		const {
			form,
			editorEmpty,
			editorError,
		} = this.state;
		const {
			classes,
			error,
			success
		} = this.props;

		return success ? (
			<div className="form__success">
				Command has been added successfully!
				<br/>
				<AddIcon onClick={this.handleReset}/>
			</div>
		) : (
			<div>
				<div className="form__group">
					<FormControl fullWidth className="form__row">
						<TextValidator
							label="Name"
							name="name"
							className="form__input"
							margin="normal"
							autoComplete="off"
							value={form.name}
							onChange={this.handleChange}
							validators={['required', 'minLength:3']}
							errorMessages={['This field is required', 'Min length - 3']}
						/>
					</FormControl>
					<FormControl fullWidth className="form__row">
						<TextValidator
							label="Description"
							name="description"
							className="form__input"
							margin="normal"
							autoComplete="off"
							value={form.description}
							onChange={this.handleChange}
						/>
					</FormControl>
					<FormControl fullWidth className="form__row">
						<InputLabel>Language</InputLabel>
						{this._renderSelectLangs()}
					</FormControl>
				</div>
				<FormControl fullWidth className="form__row">
					<Editor
						handleCustomChange={this.handleEditorChange}
						handleCustomError={this.handleEditorError}
						empty={editorEmpty}
						selectedLang={form.lang}
					/>
					<FormHelperText className={classes.formError + ' form__error'}>
						{error}
					</FormHelperText>
				</FormControl>
				<FormControl className="form__row form__row--submit">
					<Button type="submit" raised color="primary" className="form__submit">
						Done
						<Done className={classes.rightIcon} />
					</Button>
				</FormControl>
			</div>
		);
	}

	render() {
		const { form } = this.state;

		return (
			<Form
				ref="form"
				onSubmit={this.handleSubmit}
				formData={form}
			>
				{this._renderForm()}
			</Form>
		);
	}
}

const mapStateToProps = ({ commands, langs }, ownProps) => {
	return {
		pending: commands.add.pending,
		error: commands.add.error,
		success: commands.add.success,
		langs: langs.data,
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addCommandReset: () => {
			dispatch(Actions.addCommandReset());
		},
		addCommand: (data) => {
			dispatch(Actions.addCommand(data));
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddCommandForm));
