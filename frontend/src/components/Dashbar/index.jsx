import React from 'react';

import './style.scss';

export const Dashbar = (props) => {
	const {
		title,
		button: Button
	} = props;

	return (
		<div className="dashbar">
			<h3 className="dashbar__title">
				{title}
			</h3>
			<div className="dashbar__button">
				{Button}
			</div>
		</div>
	);
}

export default Dashbar;
