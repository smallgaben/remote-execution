export const APP = {
	title: 'Remex',
	description: 'Remote execution',
};

export const API = {
	host: '',
	url: '/api/'
};

export const EXEC_STATUSES = {
	EXECUTING: 'EXECUTING',
	PAUSED: 'paused',
	STOPPED: 'stopped',
	FINISHED: 'FINISHED',
	FAILED: 'FAILED',
};

export default {
	app: APP,
	api: API
};
