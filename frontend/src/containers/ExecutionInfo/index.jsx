import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import BackIcon from 'material-ui-icons/ChevronLeft';
import { FormControl } from 'material-ui/Form';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import PlayIcon from 'material-ui-icons/PlayArrow';
import PauseIcon from 'material-ui-icons/Pause';
import StopIcon from 'material-ui-icons/Stop';

import ExecStatus from 'components/ExecStatus';
import ExecActions from 'components/ExecActions';
import UILoader from 'components/UILoader';
import Editor from 'components/Editor';
import { getExecutionById } from 'redux/reducers/executions';
import Actions from 'redux/actions/executions';
import './style.scss';

class ExecutionInfo extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		const { getExecution, id } = this.props;

		getExecution(id);
	}

	_normalizeDateNumber(number) {
		if (number < 10) {
			return `0${number}`;
		}

		return number;
	}

	_renderDate(timestamp) {
		let date = new Date(timestamp);
		let month = this._normalizeDateNumber(date.getMonth() + 1);
		let hours = this._normalizeDateNumber(date.getHours());
		let minutes = this._normalizeDateNumber(date.getMinutes());
		let seconds = this._normalizeDateNumber(date.getSeconds());

		let fulldate = `${date.getDate()}.${month}`;
		fulldate += ` ${hours}:${minutes}:${seconds}`;

		return fulldate;
	}

	_renderOutput() {
		const { execution } = this.props;

		if (!execution || !execution.executionOutput) {
			return;
		}

		return (
			<div>
				{execution.executionOutput.output.map(
					(output, i) => {
						return (
							<div key={i} className="execution-console__row">
								{output.content}
								<div className="execution-console__timestamp">
									{this._renderDate(output.timestamp)}
								</div>
							</div>
						);
					}
				)}
			</div>
		);
	}

	render() {
		let { execution, success, pending } = this.props;
		let successClass = '';

		if (!success) {
			execution = {
				id: null,
				command: {}
			}
		} else {
			successClass = 'execution-info--loaded';
		}

		return (
			<div className={`execution-info ${successClass}`}>
				<UILoader className="execution-info__loader"/>
				<div className="execution-info__stats">
					<Table className="table">
						<TableHead className="table__head">
							<TableRow>
								<TableCell className="table__cell" numeric>ID</TableCell>
								<TableCell className="table__cell">Command</TableCell>
								<TableCell className="table__cell">Lang</TableCell>
								<TableCell className="table__cell">Status</TableCell>
								<TableCell className="table__cell">Actions</TableCell>
							</TableRow>
						</TableHead>
						<TableBody className="table__body">
							<TableRow className="table__row">
								<TableCell className="table__cell" numeric>{execution.id}</TableCell>
								<TableCell className="table__cell">{execution.command.name}</TableCell>
								<TableCell className="table__cell">
									{execution.command.lang}
								</TableCell>
								<TableCell className="table__cell">
									<ExecStatus status={execution.status} />
								</TableCell>
								<TableCell className="table__cell">
									<ExecActions />
								</TableCell>
							</TableRow>
						</TableBody>
					</Table>
				</div>
				<div className="execution-info__code">
					<Typography type="title">
						Code
					</Typography>
					<Editor value={execution.command.code} readOnly={true} selectedLang={execution.command.lang}/>
				</div>
				<div className="execution-info__output">
					<Typography type="title">
						Output
					</Typography>
					<div className="execution-console">
						{this._renderOutput()}
					</div>
				</div>
				<div className="execution-info__footer">
					<Link to="/dashboard/executions">
						<Button type="submit" raised color="accent">
							Back
							<BackIcon className="execution-info__back-icon"/>
						</Button>
					</Link>
				</div>
			</div>
		);
	}
}

const mapStateToProps = ({ executions }, { id }) => {
	return {
		execution: executions.get.data,
		pending: executions.get.pending,
		success: executions.get.success,
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		getExecution: (executionId) => {
			dispatch(Actions.getExecution(executionId));
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ExecutionInfo);
