import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import { Link } from 'react-router-dom';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import EditIcon from 'material-ui-icons/Settings';

import Actions from 'redux/actions/commands';
import './style.scss';

class CommandsTable extends Component {
	_renderCommands() {
		const { commands } = this.props;

		return (
			<div className="commands-table">
				<Table>
					<TableHead className="commands-table__head">
						<TableRow>
							<TableCell className="commands-table__cell" numeric>ID</TableCell>
							<TableCell className="commands-table__cell">Name</TableCell>
							<TableCell className="commands-table__cell">Description</TableCell>
							<TableCell className="commands-table__cell">Language</TableCell>
							<TableCell className="commands-table__cell">Actions</TableCell>
						</TableRow>
					</TableHead>
					<TableBody className="commands-table__body">
						{commands.map((c) => {
							return (
								<TableRow className="commands-table__row" key={c.id}>
									<TableCell className="commands-table__cell" numeric>{c.id}</TableCell>
									<TableCell className="commands-table__cell">{c.name}</TableCell>
									<TableCell className="commands-table__cell">{c.description}</TableCell>
									<TableCell className="commands-table__cell">{c.lang}</TableCell>
									<TableCell className="commands-table__cell">
										<div className="commands-table__actions">
											<Link to={`/dashboard/edit-command/${c.id}`}>
												<EditIcon />
											</Link>
										</div>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</div>
		);
	}

	render() {
		return (
			<div className="commands">
				{this._renderCommands()}
			</div>
		);
	}
}

const mapStateToProps = ({ commands }, ownProps) => {
	return {
		commands: commands.all.data,
		pending: commands.all.pending,
		error: commands.all.error,
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		getCommands: () => {
			dispatch(Actions.getCommands());
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(CommandsTable);
