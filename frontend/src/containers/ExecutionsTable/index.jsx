import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import { Link } from 'react-router-dom';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';

import Actions from 'redux/actions/executions';
import ExecStatus from 'components/ExecStatus';
import ExecActions from 'components/ExecActions';
import './style.scss';

class ExecutionsTable extends Component {
	_renderExecutions() {
		const { executions } = this.props;

		return (
			<div className="executions-table">
				<Table>
					<TableHead className="executions-table__head">
						<TableRow>
							<TableCell className="executions-table__cell" numeric>ID</TableCell>
							<TableCell className="executions-table__cell">Command</TableCell>
							<TableCell className="executions-table__cell">Language</TableCell>
							<TableCell className="executions-table__cell">Status</TableCell>
							<TableCell className="executions-table__cell">Actions</TableCell>
						</TableRow>
					</TableHead>
					<TableBody className="executions-table__body">
						{executions.map((c) => {
							return (
								<TableRow className="executions-table__row" key={c.id}>
									<TableCell className="executions-table__cell" numeric>{c.id}</TableCell>
									<TableCell className="executions-table__cell">{c.command.name}</TableCell>
									<TableCell className="executions-table__cell">{c.command.lang}</TableCell>
									<TableCell className="executions-table__cell">
										<ExecStatus status={c.status} />
									</TableCell>
									<TableCell className="executions-table__cell">
										<ExecActions
											id={c.id}
											link={`/dashboard/execution/${c.id}`}
										/>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</div>
		);
	}

	render() {
		return (
			<div className="executions">
				{this._renderExecutions()}
			</div>
		);
	}
}

const mapStateToProps = ({ executions }, ownProps) => {
	return {
		executions: executions.all.data,
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		getExecutions: () => {
			dispatch(Actions.getExecutions());
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ExecutionsTable);
