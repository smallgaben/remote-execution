import fetch from 'isomorphic-fetch';

import BasicApi from './index';
import { API } from 'config';

class CommandsApi extends BasicApi {
	static getCommands() {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}commands`, {
			method: 'GET',
			headers: headers
		})
		.then(this._handleResponse);
	}

	static addCommand({ name, description, lang, code }) {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}commands`, {
			method: 'POST',
			headers: headers,
			body: JSON.stringify({
				name,
				description,
				lang,
				code
			})
		})
		.then(this._handleResponse);
	}
}

export default CommandsApi;
