import fetch from 'isomorphic-fetch';

import BasicApi from './index';
import { API } from 'config';

class ExecutionsApi extends BasicApi {
	static getExecutions() {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}executions`, {
			method: 'GET',
			headers: headers
		})
		.then(this._handleResponse);
	}

	static getExecution(executionId) {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}executions/${executionId}`, {
			method: 'GET',
			headers: headers
		})
		.then(this._handleResponse);
	}

	static addExecution(commandId) {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}executions`, {
			method: 'POST',
			headers: headers,
			body: JSON.stringify({
				id: commandId
			})
		})
		.then(this._handleResponse);
	}

	static suspendExecution(executionId) {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}executions/${executionId}/suspend`, {
			method: 'POST',
			headers: headers
		})
		.then(this._handleResponse);
	}

	static resumeExecution(executionId) {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}executions/${executionId}/resume`, {
			method: 'GET',
			headers: headers
		})
		.then(this._handleResponse);
	}

	static abortExecution(executionId) {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}executions/${executionId}/abortExecution`, {
			method: 'GET',
			headers: headers
		})
		.then(this._handleResponse);
	}
}

export default ExecutionsApi;
