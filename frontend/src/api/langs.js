import fetch from 'isomorphic-fetch';

import BasicApi from './index';
import { API } from 'config';

class LangsApi extends BasicApi {
	static getLangs() {
		const headers = this.getAuthorizedRequestHeaders();

		return fetch(`${API.url}commands/languages`, {
			method: 'GET',
			headers: headers
		})
		.then(this._handleResponse);
	}
}

export default LangsApi;
