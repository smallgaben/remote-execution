import { API } from 'config';

class BasicApi {
	static getRequestHeaders() {
		return {
			'Content-Type': 'application/json;charset=UTF-8'
		};
	}

	static getAuthorizedRequestHeaders() {
		return {
			'Content-Type': 'application/json;charset=UTF-8',
			'Authorization': `Bearer ${localStorage.jwt}`
		};
	}

	static _handleResponse(response) {
		if (response.ok || response.status === 500 || response.status === 400) {
			return response.json();
		}

		throw new Error("Oops... Something went wrong");
	}
}

export default BasicApi;
