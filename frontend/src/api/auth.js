import fetch from 'isomorphic-fetch';

import BasicApi from './index';
import { API } from 'config';

class AuthApi extends BasicApi {
	static signIn({ username, password }) {
		const headers = this.getRequestHeaders();

		return fetch(`${API.url}auth`, {
			method: 'POST',
			headers: headers,
			body: JSON.stringify({
				username,
				password
			})
		})
		.then(this._handleResponse);
	}

	static signUp({ username, password, email }) {
		const headers = this.getRequestHeaders();

		return fetch(`${API.url}users`, {
			method: 'POST',
			headers: headers,
			body: JSON.stringify({
				username,
				password,
				email
			})
		})
		.then(this._handleResponse);
	}
}

export default AuthApi;
