import { push } from 'react-router-redux';
import CommandsApi from 'api/commands';

export const GET_COMMANDS_REQUEST = 'GET_COMMANDS_REQUEST';
export const GET_COMMANDS_SUCCESS = 'GET_COMMANDS_SUCCESS';
export const GET_COMMANDS_FAIL 	 	= 'GET_COMMANDS_FAIL';

export const ADD_COMMAND_REQUEST = 'ADD_COMMAND_REQUEST';
export const ADD_COMMAND_SUCCESS = 'ADD_COMMAND_SUCCESS';
export const ADD_COMMAND_FAIL 	 = 'ADD_COMMAND_FAIL';
export const ADD_COMMAND_RESET 	 = 'ADD_COMMAND_RESET';

const requestGetCommands = () => {
	return {
		type: GET_COMMANDS_REQUEST,
		payload: {}
	}
}

const successGetCommands = (data) => {
	return {
		type: GET_COMMANDS_SUCCESS,
		payload: {
			data
		}
	}
}

const failGetCommands = (error) => {
	return {
		type: GET_COMMANDS_FAIL,
		payload: {
			error
		}
	}
}

export function getCommands() {
  return (dispatch, getStore) => {
		const session = getStore().auth.session;

		if (!session) {
			return;
		}

		dispatch(requestGetCommands());

    return CommandsApi.getCommands()
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failGetCommands(json.message));
				} else if (json.errorMessage) {
					dispatch(failGetCommands(json.errorMessage));
				} else {
					dispatch(successGetCommands(json));
				}
			})
			.catch(err => {
				return dispatch(failGetCommands(err.message));
			});
  }
}

const requestAddCommand = () => {
	return {
		type: ADD_COMMAND_REQUEST,
		payload: {}
	}
}

const successAddCommand = (data) => {
	return {
		type: ADD_COMMAND_SUCCESS,
		payload: {
			data
		}
	}
}

const failAddCommand = (error) => {
	return {
		type: ADD_COMMAND_FAIL,
		payload: {
			error
		}
	}
}

export function addCommandReset() {
	return {
		type: ADD_COMMAND_RESET,
		payload: {}
	}
}

export function addCommand(data) {
  return (dispatch) => {
		dispatch(requestAddCommand());

    return CommandsApi.addCommand(data)
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failAddCommand(json.message));
				} else if (json.errorMessage) {
					dispatch(failAddCommand(json.errorMessage));
				} else {
					const newCommand = {
						...data,
						id: json.id
					};
					dispatch(successAddCommand(json));
					dispatch(successGetCommands([newCommand]));
				}
			})
			.catch(err => {
				return dispatch(failAddCommand(err.message));
			});
  }
}

export default {
	getCommands,
	addCommand,
	addCommandReset,
};
