import { push } from 'react-router-redux';
import LangsApi from 'api/langs';

export const GET_LANGS_REQUEST = 'GET_LANGS_REQUEST';
export const GET_LANGS_SUCCESS = 'GET_LANGS_SUCCESS';
export const GET_LANGS_FAIL 	 = 'GET_LANGS_FAIL';

const requestGetLangs = () => {
	return {
		type: GET_LANGS_REQUEST,
		payload: {}
	}
}

const successGetLangs = (data) => {
	return {
		type: GET_LANGS_SUCCESS,
		payload: {
			data
		}
	}
}

const failGetLangs = (error) => {
	return {
		type: GET_LANGS_FAIL,
		payload: {
			error
		}
	}
}

const prettifyLangs = (langs) => {
	return langs.map(
		(lang) => {
			let title = lang.replace(/_/, '').toLowerCase();
			return {
				value: lang,
				title
			}
		}
	)
}

export function getLangs() {
  return (dispatch, getStore) => {
		const session = getStore().auth.session;

		if (!session) {
			return;
		}

		dispatch(requestGetLangs());

    return LangsApi.getLangs()
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failGetLangs(json.message));
				} else if (json.errorMessage) {
					dispatch(failGetLangs(json.errorMessage));
				} else {
					dispatch(successGetLangs(prettifyLangs(json)));
				}
			})
			.catch(err => {
				return dispatch(failGetLangs(err.message));
			});
  }
}

export default {
	getLangs
};
