import { push } from 'react-router-redux';
import ExecutionsApi from 'api/executions';

export const GET_EXECUTIONS_REQUEST = 'GET_EXECUTIONS_REQUEST';
export const GET_EXECUTIONS_SUCCESS = 'GET_EXECUTIONS_SUCCESS';
export const GET_EXECUTIONS_FAIL 	  = 'GET_EXECUTIONS_FAIL';

export const GET_EXECUTION_REQUEST = 'GET_EXECUTION_REQUEST';
export const GET_EXECUTION_SUCCESS = 'GET_EXECUTION_SUCCESS';
export const GET_EXECUTION_FAIL 	 = 'GET_EXECUTION_FAIL';

export const ADD_EXECUTION_REQUEST = 'ADD_EXECUTION_REQUEST';
export const ADD_EXECUTION_SUCCESS = 'ADD_EXECUTION_SUCCESS';
export const ADD_EXECUTION_FAIL 	 = 'ADD_EXECUTION_FAIL';
export const ADD_EXECUTION_RESET 	 = 'ADD_EXECUTION_RESET';

export const HANDLE_EXECUTION_REQUEST  = 'HANDLE_EXECUTION_REQUEST';
export const SUSPEND_EXECUTION_SUCCESS = 'SUSPEND_EXECUTION_SUCCESS';
export const RESUME_EXECUTION_SUCCESS  = 'RESUME_EXECUTION_SUCCESS';
export const ABORT_EXECUTION_SUCCESS   = 'ABORT_EXECUTION_SUCCESS';

const requestGetExecutions = () => {
	return {
		type: GET_EXECUTIONS_REQUEST,
		payload: {}
	}
}

const successGetExecutions = (data) => {
	return {
		type: GET_EXECUTIONS_SUCCESS,
		payload: {
			data
		}
	}
}

const failGetExecutions = (error) => {
	return {
		type: GET_EXECUTIONS_FAIL,
		payload: {
			error
		}
	}
}

export function getExecutions() {
  return (dispatch, getStore) => {
		const session = getStore().auth.session;

		if (!session) {
			return;
		}

		dispatch(requestGetExecutions());

    return ExecutionsApi.getExecutions()
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failGetExecutions(json.message));
				} else if (json.errorMessage) {
					dispatch(failGetExecutions(json.errorMessage));
				} else {
					dispatch(successGetExecutions(json));
				}
			})
			.catch(err => {
				return dispatch(failGetExecutions(err.message));
			});
  }
}

const requestAddExecution = () => {
	return {
		type: ADD_EXECUTION_REQUEST,
		payload: {}
	}
}

const successAddExecution = (commandId) => {
	return {
		type: ADD_EXECUTION_SUCCESS,
		payload: {
			commandId
		}
	}
}

const failAddExecution = (error) => {
	return {
		type: ADD_EXECUTION_FAIL,
		payload: {
			error
		}
	}
}

export function addExecutionReset() {
	return {
		type: ADD_EXECUTION_RESET,
		payload: {}
	}
}

export function addExecution(commandId) {
  return (dispatch) => {
		dispatch(requestAddExecution());

    return ExecutionsApi.addExecution(commandId)
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failAddExecution(json.message));
				} else if (json.errorMessage) {
					dispatch(failAddExecution(json.errorMessage));
				} else {
					dispatch(successAddExecution(json));
					dispatch(getExecution(json));
				}
			})
			.catch(err => {
				return dispatch(failAddExecution(err.message));
			});
  }
}

const requestHandleExecution = () => {
	return {
		type: HANDLE_EXECUTION_REQUEST,
		payload: {}
	}
}

const successSuspendExecution = (data) => {
	return {
		type: SUSPEND_EXECUTION_SUCCESS,
		payload: {
			data
		}
	}
}

const successResumeExecution = (data) => {
	return {
		type: RESUME_EXECUTION_SUCCESS,
		payload: {
			data
		}
	}
}

const successAbortExecution = (data) => {
	return {
		type: ABORT_EXECUTION_SUCCESS,
		payload: {
			data
		}
	}
}

export function suspendExecution(executionId) {
  return (dispatch) => {
		dispatch(requestHandleExecution());

    return ExecutionsApi.suspendExecution(executionId)
			.then(json => {
				if (json.status && json.status == 500) {
					return false;
				} else if (json.errorMessage) {
					return false;
				} else {
					dispatch(successSuspendExecution(json));
				}
			})
			.catch(err => {
				return false;
			});
  }
}

export function resumeExecution(executionId) {
  return (dispatch) => {
		dispatch(requestHandleExecution());

    return ExecutionsApi.resumeExecution(executionId)
			.then(json => {
				if (json.status && json.status == 500) {
					return false;
				} else if (json.errorMessage) {
					return false;
				} else {
					dispatch(successResumeExecution(json));
				}
			})
			.catch(err => {
				return false;
			});
  }
}

export function abortExecution(executionId) {
  return (dispatch) => {
		dispatch(requestHandleExecution());

    return ExecutionsApi.abortExecution(executionId)
			.then(json => {
				if (json.status && json.status == 500) {
					return false;
				} else if (json.errorMessage) {
					return false;
				} else {
					dispatch(successAbortExecution(json));
				}
			})
			.catch(err => {
				return false;
			});
  }
}

const requestGetExecution = () => {
	return {
		type: GET_EXECUTION_REQUEST,
		payload: {}
	}
}

const successGetExecution = (data) => {
	return {
		type: GET_EXECUTION_SUCCESS,
		payload: {
			data
		}
	}
}

const failGetExecution = (error) => {
	return {
		type: GET_EXECUTION_FAIL,
		payload: {
			error
		}
	}
}

export function getExecution(executionId) {
  return (dispatch, getStore) => {
		const session = getStore().auth.session;

		if (!session) {
			return;
		}

		dispatch(requestGetExecution());

    return ExecutionsApi.getExecution(executionId)
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failGetExecution(json.message));
				} else if (json.errorMessage) {
					dispatch(failGetExecution(json.errorMessage));
				} else {
					dispatch(successGetExecution(json));
				}
			})
			.catch(err => {
				return dispatch(failGetExecution(err.message));
			});
  }
}

export default {
	getExecutions,
	getExecution,
	addExecution,
	addExecutionReset,
	suspendExecution,
	resumeExecution,
	abortExecution,
};
