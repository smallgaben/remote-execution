import { push } from 'react-router-redux';
import AuthApi from 'api/auth';
import { getLangs } from 'redux/actions/langs';
import { getCommands } from 'redux/actions/commands';
import { getExecutions } from 'redux/actions/executions';

export const SIGNIN_REQUEST = 'SIGNIN_REQUEST';
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const SIGNIN_FAIL 	 	= 'SIGNIN_FAIL';
export const SIGNIN_RESET 	= 'SIGNIN_RESET';

export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const SIGNUP_REQUEST = 'SIGNUP_REQUEST';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_FAIL 		= 'SIGNUP_FAIL';
export const SIGNUP_RESET 	= 'SIGNUP_RESET';

const requestSignIn = () => {
	return {
		type: SIGNIN_REQUEST,
		payload: {}
	}
}

const successSignIn = (data) => {
	return {
		type: SIGNIN_SUCCESS,
		payload: {
			session: !!localStorage.jwt
		}
	}
}

const failSignIn = (error) => {
	return {
		type: SIGNIN_FAIL,
		payload: {
			error
		}
	}
}

export function signIn(credentials) {
  return (dispatch) => {
		dispatch(requestSignIn());

    return AuthApi.signIn(credentials)
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failSignIn(json.message));
				} else if (json.errorMessage) {
					dispatch(failSignIn(json.errorMessage));
				} else {
					localStorage.setItem('jwt', json.token);
					dispatch(successSignIn(json));
					dispatch(getLangs());
					dispatch(getCommands());
					dispatch(getExecutions());
					dispatch(push('/dashboard'));
				}
			})
			.catch(err => {
				return dispatch(failSignIn(err.message));
			});
  }
}

export function signInReset() {
	return {
		type: SIGNIN_RESET,
		payload: {}
	}
}

const requestSignUp = () => {
	return {
		type: SIGNUP_REQUEST,
		payload: {}
	}
}

const successSignUp = (data) => {
	return {
		type: SIGNUP_SUCCESS,
		payload: {}
	}
}

const failSignUp = (error) => {
	return {
		type: SIGNUP_FAIL,
		payload: {
			error
		}
	}
}

export function signUp(credentials) {
  return (dispatch) => {
		dispatch(requestSignUp());

    return AuthApi.signUp(credentials)
			.then(json => {
				if (json.status && json.status == 500) {
					dispatch(failSignUp(json.message));
				} else if (json.errorMessage) {
					dispatch(failSignUp(json.errorMessage));
				} else {
					dispatch(successSignUp(json));
				}
			})
			.catch(err => {
				return dispatch(failSignUp(err.message));
			});
  }
}

export function signUpReset() {
	return {
		type: SIGNUP_RESET,
		payload: {}
	}
}

const successLogout = () => {
	return {
		type: LOGOUT_SUCCESS,
		payload: {}
	}
}

export function logout() {
  return (dispatch) => {
		localStorage.removeItem('jwt');
		dispatch(successLogout());
		dispatch(push('/home'));
  }
}

export default {
	signIn,
	signInReset,
	signUp,
	signUpReset,
	logout
};
