import {
	GET_COMMANDS_REQUEST,
	GET_COMMANDS_SUCCESS,
	GET_COMMANDS_FAIL,
	ADD_COMMAND_REQUEST,
	ADD_COMMAND_SUCCESS,
	ADD_COMMAND_FAIL,
	ADD_COMMAND_RESET,
} from 'redux/actions/commands';
import { combineReducers } from 'redux';
import { LOGOUT_SUCCESS } from 'redux/actions/auth';

import { merge } from 'utils';

const commandsDefault = {
	pending: false,
	error: false,
	data: [],
};

const commands = (state = commandsDefault, action) => {
	switch (action.type) {
		case GET_COMMANDS_REQUEST:
			return {
				...state,
				pending: true,
				error: false
			}
			break;
		case GET_COMMANDS_SUCCESS:
			return {
				pending: false,
				error: false,
				data: merge(state.data, action.payload.data)
			}
			break;
		case GET_COMMANDS_FAIL:
			return {
				...state,
				pending: false,
				error: action.payload.error
			}
			break;
		case LOGOUT_SUCCESS:
			return {
				...commandsDefault
			};
			break;
		default:
			return state;
	}
};

const addCommandDefault = {
	pending: false,
	error: false,
	success: false,
	data: [],
};

const addCommand = (state = addCommandDefault, action) => {
	switch (action.type) {
		case ADD_COMMAND_REQUEST:
			return {
				...state,
				pending: true,
				error: false,
				success: false,
			}
			break;
		case ADD_COMMAND_FAIL:
			return {
				...state,
				pending: false,
				error: action.payload.error,
				success: false,
			}
			break;
		case ADD_COMMAND_SUCCESS:
			return {
				pending: false,
				error: false,
				success: true,
			}
			break;
		case ADD_COMMAND_RESET:
			return addCommandDefault;
			break;
		default:
			return state;
	}
}

const commandsReducer = combineReducers({
	all: commands,
	add: addCommand
})

export function getCommandById(state = [], id) {
	let command = state.filter(command => command.id == id);

	return command[0] || {};
}

export default commandsReducer;
