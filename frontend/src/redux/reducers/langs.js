import {
	GET_LANGS_REQUEST,
	GET_LANGS_SUCCESS,
	GET_LANGS_FAIL,
} from 'redux/actions/langs';

import { merge } from 'utils';

const initialState = {
	pending: false,
	error: false,
	data: [],
};

const langs = (state = initialState, action) => {
	switch (action.type) {
		case GET_LANGS_REQUEST:
			return {
				...state,
				pending: true,
				error: false
			}
			break;
		case GET_LANGS_SUCCESS:
			return {
				pending: false,
				error: false,
				data: action.payload.data
			}
			break;
		case GET_LANGS_FAIL:
			return {
				...state,
				pending: false,
				error: action.payload.error
			}
			break;
		default:
			return state;
	}
};

export default langs;
