import {
	SIGNUP_REQUEST,
	SIGNUP_SUCCESS,
	SIGNUP_FAIL,
	SIGNUP_RESET
} from 'redux/actions/auth';

const defaultState = {
	pending: false,
	error: false,
	success: false
};

const signUp = (state = defaultState, action) => {
	switch (action.type) {
		case SIGNUP_REQUEST:
			return {
				pending: true,
				error: false,
				success: false
			}
			break;
		case SIGNUP_SUCCESS:
			return {
				pending: false,
				error: false,
				success: true
			}
			break;
		case SIGNUP_FAIL:
			return {
				pending: false,
				error: action.payload.error,
				success: false
			}
			break;
		case SIGNUP_RESET:
			return {
				pending: false,
				error: false,
				success: false
			}
		default:
			return state;
	}
};

export default signUp;
