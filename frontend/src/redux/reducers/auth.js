import {
	SIGNIN_REQUEST,
	SIGNIN_SUCCESS,
	SIGNIN_RESET,
	SIGNIN_FAIL,
	LOGOUT_SUCCESS
} from 'redux/actions/auth';

const defaultState = {
	session: !!localStorage.jwt,
	pending: false,
	error: false,
	user: null
};

const auth = (state = defaultState, action) => {
	switch (action.type) {
		case SIGNIN_REQUEST:
			return {
				...state,
				pending: true,
				error: false
			}
			break;
		case SIGNIN_SUCCESS:
			return {
				pending: false,
				error: false,
				session: true,
				user: {}
			}
			break;
		case SIGNIN_FAIL:
			return {
				...state,
				pending: false,
				error: action.payload.error
			}
			break;
		case SIGNIN_RESET:
			return {
				...state,
				pending: false,
				error: false
			}
			break;
		case LOGOUT_SUCCESS:
			return {
				session: false,
				pending: false,
				error: false,
				user: null
			}
			break;
		default:
			return state;
	}
};

export default auth;
