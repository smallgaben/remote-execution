import {
	GET_EXECUTIONS_REQUEST,
	GET_EXECUTIONS_SUCCESS,
	GET_EXECUTIONS_FAIL,
	GET_EXECUTION_REQUEST,
	GET_EXECUTION_SUCCESS,
	GET_EXECUTION_FAIL,
	ADD_EXECUTION_REQUEST,
	ADD_EXECUTION_SUCCESS,
	ADD_EXECUTION_FAIL,
	ADD_EXECUTION_RESET,
	HANDLE_EXECUTION_REQUEST,
	SUSPEND_EXECUTION_SUCCESS,
	RESUME_EXECUTION_SUCCESS,
	ABORT_EXECUTION_SUCCESS,
} from 'redux/actions/executions';
import { combineReducers } from 'redux';
import { LOGOUT_SUCCESS } from 'redux/actions/auth';

import { merge } from 'utils';

const executionsDefault = {
	pending: false,
	error: false,
	data: [],
};

const executions = (state = executionsDefault, action) => {
	switch (action.type) {
		case GET_EXECUTIONS_REQUEST:
			return {
				...state,
				pending: true,
				error: false
			}
			break;
		case GET_EXECUTIONS_SUCCESS:
			return {
				pending: false,
				error: false,
				data: merge(state.data, action.payload.data)
			}
			break;
		case GET_EXECUTIONS_FAIL:
			return {
				...state,
				pending: false,
				error: action.payload.error
			}
			break;
		case GET_EXECUTION_SUCCESS:
			return {
				...state,
				data: merge(state.data, [action.payload.data])
			}
			break;
		case LOGOUT_SUCCESS:
			return {
				...executionsDefault
			};
			break;
		default:
			return state;
	}
};

const addExecutionDefault = {
	pending: false,
	error: false,
	success: false,
	data: [],
};

const addExecution = (state = addExecutionDefault, action) => {
	switch (action.type) {
		case ADD_EXECUTION_REQUEST:
			return {
				...state,
				pending: true,
				error: false,
				success: false,
			}
			break;
		case ADD_EXECUTION_FAIL:
			return {
				...state,
				pending: false,
				error: action.payload.error,
				success: false,
			}
			break;
		case ADD_EXECUTION_SUCCESS:
			return {
				pending: false,
				error: false,
				success: true,
			}
			break;
		case ADD_EXECUTION_RESET:
			return addExecutionDefault;
			break;
		default:
			return state;
	}
}

const getExecutionDefault = {
	pending: false,
	error: false,
	success: false,
	data: null,
};

const getExecution = (state = getExecutionDefault, action) => {
	switch (action.type) {
		case GET_EXECUTION_REQUEST:
			return {
				...state,
				pending: true,
				error: false,
				success: false,
			}
			break;
		case GET_EXECUTION_FAIL:
			return {
				...state,
				pending: false,
				error: action.payload.error,
				success: false,
			}
			break;
		case GET_EXECUTION_SUCCESS:
			return {
				pending: false,
				error: false,
				success: true,
				data: action.payload.data
			}
			break;
		default:
			return state;
	}
}


const executionsReducer = combineReducers({
	all: executions,
	add: addExecution,
	get: getExecution,
})

export function getExecutionById(state = [], id) {
	let execution = state.filter(execution => execution.id == id);

	return execution[0] || {};
}

export default executionsReducer;
