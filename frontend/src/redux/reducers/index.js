import { combineReducers } from 'redux';
import auth from './auth';
import signup from './signUp';
import commands from './commands';
import executions from './executions';
import langs from './langs';

const rootReducer = combineReducers({
	auth,
	signup,
	commands,
	executions,
	langs,
})

export default rootReducer;
