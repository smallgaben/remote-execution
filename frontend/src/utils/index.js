export const merge = (base, updated) => {
	let map = {};

  base.forEach(function(v) {
    map[v.id] = v;
  })

  updated.forEach(function(v) {
    map[v.id] = v;
  })

  const out = [];

  for (let id in map) {
    if (map.hasOwnProperty(id))
      out.push(map[id]);
  }

  return out;
};
