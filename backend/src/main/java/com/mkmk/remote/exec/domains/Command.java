package com.mkmk.remote.exec.domains;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "commands")
public class Command {

    @Id
    @GenericGenerator(
        name = "commandSequenceGenerator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
            @Parameter(name = "sequence_name", value = "commands_id_seq"),
            @Parameter(name = "initial_value", value = "0"),
            @Parameter(name = "increment_size", value = "1"),
        }
    )
    @GeneratedValue(generator = "commandSequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "code")
    private String code;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "language")
    private Language lang;

    public Command() {
    }

    public Command(Long id, String name, String description,
                   String code, User user, Language lang) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.code = code;
        this.user = this.user;
        this.lang = lang;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Command command = (Command) o;
        return Objects.equals(id, command.id) &&
            Objects.equals(name, command.name) &&
            Objects.equals(description, command.description) &&
            Objects.equals(code, command.code) &&
            Objects.equals(user, command.user) &&
            lang == command.lang;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, code, user, lang);
    }

    @Override
    public String toString() {
        return "Command{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", code='" + code + '\'' +
            ", user=" + user +
            ", lang=" + lang +
            '}';
    }
}
