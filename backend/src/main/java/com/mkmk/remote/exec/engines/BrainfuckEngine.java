package com.mkmk.remote.exec.engines;

import com.google.common.base.Charsets;
import com.mkmk.remote.exec.exceptions.ExecutionException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.stream.Collectors;

public class BrainfuckEngine implements CustomScriptEngine {

    protected byte[] data;
    protected int dataPointer;
    protected int charPointer;
    protected int lineCount;
    protected int columnCount;
    protected Reader consoleReader;
    protected Writer outWriter;

    public BrainfuckEngine(int cells) {
        this(cells, new PrintWriter(System.out), System.in);
    }

    public BrainfuckEngine(int cells, Writer out) {
        this(cells, out, System.in);
    }

    public BrainfuckEngine(int cells, Writer out, InputStream in) {
        init(cells);
        outWriter = out;
        consoleReader = new InputStreamReader(in, Charsets.UTF_8);
    }

    protected void init(int cells) {
        data = new byte[cells];
        dataPointer = 0;
        charPointer = 0;
    }

    public void eval(File file) {
        try {
            eval(Files.lines(file.toPath()).collect(Collectors.joining()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void eval(String str) {
        while (charPointer < str.length()) {
            eval(str.charAt(charPointer), str.toCharArray());
            charPointer++;
        }
        init(data.length);
    }

    protected void eval(char c, char[] chars) {
        switch (c) {
            case Token.NEXT:
                if ((dataPointer + 1) > data.length) {
                    throwException(lineCount, columnCount,
                        String.format("data pointer (%s) on postion %s out of range.", dataPointer, charPointer));
                }
                dataPointer++;
                break;
            case Token.PREVIOUS:
                if ((dataPointer - 1) < 0) {
                    throwException(lineCount, columnCount,
                        String.format("data pointer (%s) on postion %s negative.", dataPointer, charPointer));
                }
                dataPointer--;
                break;
            case Token.PLUS:
                data[dataPointer]++;
                break;
            case Token.MINUS:
                data[dataPointer]--;
                break;
            case Token.OUTPUT:
                try {
                    outWriter.write(data[dataPointer]);
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
                break;
            case Token.INPUT:
                try {
                    data[dataPointer] = (byte) consoleReader.read();
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
                break;
            case Token.BRACKET_LEFT:
                if (data[dataPointer] == 0) {
                    int i = 1;
                    while (i > 0) {
                        char c2 = chars[++charPointer];
                        if (c2 == Token.BRACKET_LEFT) {
                            i++;
                        } else {
                            if (c2 == Token.BRACKET_RIGHT) {
                                i--;
                            }
                        }
                    }
                }
                break;
            case Token.BRACKET_RIGHT:
                int i = 1;
                while (i > 0) {
                    char c2 = chars[--charPointer];
                    if (c2 == Token.BRACKET_LEFT) {
                        i--;
                    } else {
                        if (c2 == Token.BRACKET_RIGHT)
                            i++;
                    }
                }
                charPointer--;
                break;
            default:
                throwException(lineCount, columnCount, "unknown character.");
        }
        columnCount++;
    }

    private void throwException(int lineCount, int columnCount, String message) {
        throw new ExecutionException(
            String.format("Error on line %s, column %s: %s", lineCount, columnCount, message));
    }

    protected static class Token {

        public static final char NEXT = '>';
        public static final char PREVIOUS = '<';
        public static final char PLUS = '+';
        public static final char MINUS = '-';
        public static final char OUTPUT = '.';
        public static final char INPUT = ',';
        public static final char BRACKET_LEFT = '[';
        public static final char BRACKET_RIGHT = ']';

        private Token() {
        }
    }
}
