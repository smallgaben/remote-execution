package com.mkmk.remote.exec.controllers;

import com.mkmk.remote.exec.domains.User;
import com.mkmk.remote.exec.dto.ResourceIdDto;
import com.mkmk.remote.exec.dto.UserDto;
import com.mkmk.remote.exec.exceptions.ResourceAlreadyExistsException;
import com.mkmk.remote.exec.exceptions.rest.BadRequestException;
import com.mkmk.remote.exec.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController extends AbstractController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<ResourceIdDto> registerUser(@Valid @RequestBody UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());

        try {
            userService.create(user);
        } catch (ResourceAlreadyExistsException e) {
            throw new BadRequestException(e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(new ResourceIdDto(user.getId()));
    }
}
