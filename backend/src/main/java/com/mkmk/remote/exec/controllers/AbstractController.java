package com.mkmk.remote.exec.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api", produces = "application/json", consumes = "application/json")
public class AbstractController {
}
