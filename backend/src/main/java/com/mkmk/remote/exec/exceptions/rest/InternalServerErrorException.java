package com.mkmk.remote.exec.exceptions.rest;

public class InternalServerErrorException extends RestException {
    public InternalServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
