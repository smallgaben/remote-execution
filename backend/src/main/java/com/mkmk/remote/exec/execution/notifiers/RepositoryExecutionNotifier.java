package com.mkmk.remote.exec.execution.notifiers;

import com.mkmk.remote.exec.domains.Message;
import com.mkmk.remote.exec.repository.MessageRepository;

public class RepositoryExecutionNotifier implements ExecutionOutputNotifier {

    private MessageRepository messageRepository;

    public RepositoryExecutionNotifier(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public void notifyOutput(Message message) {
        messageRepository.addMessage(message);
    }
}
