package com.mkmk.remote.exec.config;

import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.domains.Language;
import com.mkmk.remote.exec.execution.ExecutionThreadsHolder;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;
import com.mkmk.remote.exec.execution.notifiers.RepositoryExecutionNotifier;
import com.mkmk.remote.exec.execution.threads.*;
import com.mkmk.remote.exec.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.script.ScriptEngineManager;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import static com.mkmk.remote.exec.domains.Language.*;

@Configuration
public class ApplicationConfig {

    @Bean
    public ScriptEngineManager scriptEngineManager() {
        return new ScriptEngineManager();
    }

    @Bean
    public ExecutionThreadsHolder executionThreadsHolder(ScriptEngineManager engineManager, List<ExecutionOutputNotifier> notifiers) {
        return new ExecutionThreadsHolder(new HashMap<Language, Function<Execution, ExecutionThread>>() {{
            put(JAVA_SCRIPT, (execution) -> new JavaScriptExecutionThread(execution, notifiers, engineManager.getEngineByName("js")));
            put(PYTHON, (execution) -> new PythonExecutionThread(execution, notifiers, engineManager.getEngineByName("python")));
            put(RUBY, (execution) -> new RubyExecutionThread(execution, notifiers, engineManager.getEngineByName("jruby")));
            put(BRAINFUCK, (execution) -> new BrainfuckExecutionThread(execution, notifiers));
            put(OOK, (execution) -> new OokExecutionThread(execution, notifiers));
            put(TROLL_SCRIPT, (execution) -> new TrollScriptExecutionThread(execution, notifiers));
        }});
    }

    @Bean
    public ExecutionOutputNotifier repositoryNotifier(MessageRepository messageRepository) {
        return new RepositoryExecutionNotifier(messageRepository);
    }

    @Bean
    public JedisPool connectionPool() {
        return new JedisPool(new JedisPoolConfig(), "localhost", 4444);
    }
}
