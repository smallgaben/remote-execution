package com.mkmk.remote.exec.execution.threads;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.engines.OokEngine;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;

import java.io.Writer;
import java.util.List;

public class OokExecutionThread extends ExecutionThread {

    private static final int MEMORY_CELLS = 100000;

    public OokExecutionThread(Execution execution, List<ExecutionOutputNotifier> notifiers) {
        super(execution, notifiers);
    }

    @Override
    protected void evaluate(Writer writer) {
        OokEngine engine = new OokEngine(MEMORY_CELLS, writer);
        engine.eval(getExecution().getCommand().getCode());
    }
}
