package com.mkmk.remote.exec.execution.notifiers;

import com.mkmk.remote.exec.domains.Message;

public interface ExecutionOutputNotifier {
    void notifyOutput(Message message);
}
