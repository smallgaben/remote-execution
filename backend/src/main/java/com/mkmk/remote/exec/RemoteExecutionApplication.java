package com.mkmk.remote.exec;

import com.mkmk.remote.exec.domains.User;
import com.mkmk.remote.exec.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RemoteExecutionApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(RemoteExecutionApplication.class, args);
    }
}
