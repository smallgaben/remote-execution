package com.mkmk.remote.exec.services.impl;

import com.mkmk.remote.exec.domains.User;
import com.mkmk.remote.exec.exceptions.ResourceAlreadyExistsException;
import com.mkmk.remote.exec.repository.ExecutionLifecycleManagerRepository;
import com.mkmk.remote.exec.repository.UserRepository;
import com.mkmk.remote.exec.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ExecutionLifecycleManagerRepository executionLifecycleManagerRepository;

    @Override
    public User create(User user) {
        if (userRepository.findByUsernameOrEmail(user.getUsername(), user.getEmail()).size() > 0) {
            throw new ResourceAlreadyExistsException("User with provided username or email already exists");
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        executionLifecycleManagerRepository.create(user.getUsername());
        return userRepository.save(user);
    }

    private List<User> findSimilarByUsername(String username, String email) {
        return userRepository.findByUsernameOrEmail(username, email);
    }
}
