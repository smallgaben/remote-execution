package com.mkmk.remote.exec.domains;

public class Execution {

    private Long id;
    private Command command;
    private Status status;
    private ExecutionOutput executionOutput;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ExecutionOutput getExecutionOutput() {
        return executionOutput;
    }

    public void setExecutionOutput(ExecutionOutput executionOutput) {
        this.executionOutput = executionOutput;
    }
}
