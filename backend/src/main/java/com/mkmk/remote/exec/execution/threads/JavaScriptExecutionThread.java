package com.mkmk.remote.exec.execution.threads;

import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class JavaScriptExecutionThread extends ExecutionThread {
    private static final Logger log = LoggerFactory.getLogger(JavaScriptExecutionThread.class);

    private final ScriptEngine engine;

    public JavaScriptExecutionThread(Execution execution, List<ExecutionOutputNotifier> notifiers, ScriptEngine engine) {
        super(execution, notifiers);
        this.engine = engine;
    }

    @Override
    protected void evaluate(Writer writer) {
        engine.getContext().setWriter(writer);
        engine.getContext().setErrorWriter(writer);
        try {
            engine.eval(getExecution().getCommand().getCode());
        } catch (ScriptException e) {
            log.error("Failed to run script", e);
            try {
                writer.write(e.getMessage() + "\n");
            } catch (IOException e1) {
            }
        }
    }
}
