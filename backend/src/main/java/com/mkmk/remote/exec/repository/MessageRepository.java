package com.mkmk.remote.exec.repository;

import com.mkmk.remote.exec.domains.Message;

import java.util.List;

public interface MessageRepository {

    void addMessage(Message message);

    List<Message> getByExecutionId(Long executionId);
}
