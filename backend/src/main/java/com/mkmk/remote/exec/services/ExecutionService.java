package com.mkmk.remote.exec.services;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.exceptions.ResourceNotFoundException;

import java.util.List;

public interface ExecutionService {

    Execution create(Command command);

    Execution getById(Long executionId) throws ResourceNotFoundException;

    List<Execution> getAllByUsername(String username);

    void abort(Long executionId);

    void suspend(Long executionId);

    void resume(Long executionId);
}
