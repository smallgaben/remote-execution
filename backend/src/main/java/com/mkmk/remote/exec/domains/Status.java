package com.mkmk.remote.exec.domains;

public enum Status {
    ABORTED,
    EXECUTING,
    FAILED,
    FINISHED,
    NEW,
    PAUSED
}
