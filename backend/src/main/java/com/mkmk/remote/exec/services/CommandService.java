package com.mkmk.remote.exec.services;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.exceptions.ResourceNotFoundException;

import java.util.List;

public interface CommandService {

    Command create(String username, String name, String description, String language, String code) throws IllegalArgumentException;

    Command getById(Long commandId) throws ResourceNotFoundException;

    List<Command> getAllByUsername(String username);
}
