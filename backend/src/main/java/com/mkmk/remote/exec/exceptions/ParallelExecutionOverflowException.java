package com.mkmk.remote.exec.exceptions;

public class ParallelExecutionOverflowException extends RuntimeException {
    public ParallelExecutionOverflowException(String message) {
        super(message);
    }
}
