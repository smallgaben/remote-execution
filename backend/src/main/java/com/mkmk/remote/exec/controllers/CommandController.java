package com.mkmk.remote.exec.controllers;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Language;
import com.mkmk.remote.exec.dto.CommandDto;
import com.mkmk.remote.exec.dto.ResourceIdDto;
import com.mkmk.remote.exec.exceptions.ResourceNotFoundException;
import com.mkmk.remote.exec.exceptions.rest.BadRequestException;
import com.mkmk.remote.exec.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class CommandController extends AbstractController {

    @Autowired
    private CommandService commandService;

    @PostMapping("/commands")
    public ResponseEntity<ResourceIdDto> createCommand(@Valid @RequestBody CommandDto commandDto, Principal principal) {
        try {
            Command command = commandService.create(principal.getName(), commandDto.getName(), commandDto.getDescription(),
                commandDto.getLang(), commandDto.getCode());
            return ResponseEntity.status(HttpStatus.CREATED).body(new ResourceIdDto(command.getId()));
        } catch (IllegalArgumentException e) {
            throw new BadRequestException(String.format("Language [%s] isn't supported by system", commandDto.getLang()));
        } //TODO: add static analyzing exception
    }

    @GetMapping(value = "/commands/languages")
    public ResponseEntity<List<Language>> getLanguages() {
        return ResponseEntity.ok(Arrays.asList(Language.values()));
    }

    @GetMapping("/commands/{commandId}")
    public ResponseEntity<Command> getCommand(@PathVariable Long commandId) {
        try {
            return ResponseEntity.ok(commandService.getById(commandId));
        } catch (ResourceNotFoundException e) {
            throw new BadRequestException(String.format("Command with id [%d] not found", commandId));
        }
    }

    @GetMapping("/commands")
    public ResponseEntity<List<CommandDto>> getCommands(Principal principal) {
        return ResponseEntity.ok(commandService.getAllByUsername(principal.getName()).stream()
            .map(CommandDto::new)
            .collect(Collectors.toList()));
    }
}
