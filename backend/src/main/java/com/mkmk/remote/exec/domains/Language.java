package com.mkmk.remote.exec.domains;

public enum Language {
    BRAINFUCK,
    JAVA_SCRIPT,
    OOK,
    PYTHON,
    RUBY,
    TROLL_SCRIPT;
}
