package com.mkmk.remote.exec.controllers;

import com.mkmk.remote.exec.exceptions.rest.BadRequestException;
import com.mkmk.remote.exec.exceptions.rest.InternalServerErrorException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;
import java.util.List;

@ControllerAdvice(annotations = RestController.class)
public class ExceptionHandlingController {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ExceptionResponse> badRequest(BadRequestException ex, WebRequest request) {
        return new ResponseEntity<>(convert(ex), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    private ExceptionResponse convert(Exception e) {
        ExceptionResponse exceptionResponse = new ExceptionResponse();
        exceptionResponse.setErrorMessage(e.getLocalizedMessage());

        return exceptionResponse;
    }


    private class ExceptionResponse implements Serializable {

        private String errorMessage;

        private List<String> errorDescription;

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public List<String> getErrorDescription() {
            return errorDescription;
        }

        public void setErrorDescription(List<String> errorDescription) {
            this.errorDescription = errorDescription;
        }
    }
}
