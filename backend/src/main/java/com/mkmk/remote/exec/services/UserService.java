package com.mkmk.remote.exec.services;

import com.mkmk.remote.exec.domains.User;
import com.mkmk.remote.exec.exceptions.ResourceAlreadyExistsException;

import java.util.List;

public interface UserService {

    User create(User user) throws ResourceAlreadyExistsException;
}
