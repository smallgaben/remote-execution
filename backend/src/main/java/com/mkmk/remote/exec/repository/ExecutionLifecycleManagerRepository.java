package com.mkmk.remote.exec.repository;

import com.mkmk.remote.exec.execution.ExecutionLifecycleManager;

public interface ExecutionLifecycleManagerRepository {

    ExecutionLifecycleManager create(String username);

    ExecutionLifecycleManager findByUsername(String username);
}
