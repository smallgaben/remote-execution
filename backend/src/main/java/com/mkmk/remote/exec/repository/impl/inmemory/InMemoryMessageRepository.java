package com.mkmk.remote.exec.repository.impl.inmemory;

import com.mkmk.remote.exec.domains.Message;
import com.mkmk.remote.exec.repository.MessageRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class InMemoryMessageRepository implements MessageRepository {

    private List<Message> messages = new ArrayList<>();

    @Override
    public void addMessage(Message message) {
        messages.add(message);
    }

    @Override
    public List<Message> getByExecutionId(Long executionId) {
        return messages.stream().filter(ms -> executionId.equals(ms.getExecutionId())).collect(Collectors.toList());
    }
}
