package com.mkmk.remote.exec.execution.threads;

import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.domains.Status;
import com.mkmk.remote.exec.exceptions.ExecutionException;
import com.mkmk.remote.exec.execution.ExecutionOutputWriter;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;

import java.io.Writer;
import java.util.List;

public abstract class ExecutionThread extends Thread {

    private final Execution execution;
    private final ExecutionOutputWriter executionOutputWriter;

    public ExecutionThread(Execution execution, List<ExecutionOutputNotifier> notifiers) {
        super(String.format("ExecutionThread[%s:%s:%s]",
            execution.getCommand().getUser(), execution.getCommand().getName(), execution.getId()));
        this.execution = execution;
        this.executionOutputWriter = new ExecutionOutputWriter(execution.getId(), notifiers);
    }

    @Override
    public void run() {
        try (Writer writer = executionOutputWriter) {
            execution.setStatus(Status.EXECUTING);
            evaluate(writer);
            execution.setStatus(Status.FINISHED);
        } catch (Exception e) {
            execution.setStatus(e.getMessage().contains("java.lang.ThreadDeath") ? Status.ABORTED : Status.FAILED);
        }
    }

    public Execution getExecution() {
        return execution;
    }

    protected abstract void evaluate(Writer writer) throws ExecutionException;
}
