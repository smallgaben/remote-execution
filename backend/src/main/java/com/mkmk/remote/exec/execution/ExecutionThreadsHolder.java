package com.mkmk.remote.exec.execution;

import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.domains.Language;
import com.mkmk.remote.exec.execution.threads.ExecutionThread;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class ExecutionThreadsHolder {

    private Map<Language, Function<Execution, ExecutionThread>> threadsMap = new HashMap<>();

    public ExecutionThreadsHolder(Map<Language, Function<Execution, ExecutionThread>> threadsMap) {
        this.threadsMap = threadsMap;
    }

    public ExecutionThread getExecutionThread(Execution execution) {
        return threadsMap.get(execution.getCommand().getLang()).apply(execution);
    }
}
