package com.mkmk.remote.exec.services.impl;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.domains.ExecutionOutput;
import com.mkmk.remote.exec.exceptions.ResourceNotFoundException;
import com.mkmk.remote.exec.execution.ExecutionLifecycleManager;
import com.mkmk.remote.exec.repository.ExecutionLifecycleManagerRepository;
import com.mkmk.remote.exec.repository.ExecutionRepository;
import com.mkmk.remote.exec.repository.MessageRepository;
import com.mkmk.remote.exec.services.ExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExecutionServiceImpl implements ExecutionService {

    @Autowired
    private ExecutionLifecycleManagerRepository managerRepository;
    @Autowired
    private ExecutionRepository executionRepository;
    @Autowired
    private MessageRepository messageRepository;

    @Override
    public Execution create(Command command) {
        ExecutionLifecycleManager lifecycleManager = managerRepository.findByUsername(command.getUser().getUsername());

        Execution execution = executionRepository.create(command);
        lifecycleManager.start(execution);

        return execution;
    }

    @Override
    public Execution getById(Long executionId) throws ResourceNotFoundException {
        Execution execution = executionRepository.getById(executionId);
        if (execution == null) {
            throw new ResourceNotFoundException(String.format("Execution [%d] not found", executionId));
        }

        execution.setExecutionOutput(new ExecutionOutput(messageRepository.getByExecutionId(executionId)));
        return execution;
    }

    @Override
    public List<Execution> getAllByUsername(String username) {
        return executionRepository.getAllByUsername(username);
    }

    @Override
    public void abort(Long executionId) {
        Execution execution = getById(executionId);
        ExecutionLifecycleManager lifecycleManager = managerRepository.findByUsername(execution.getCommand().getUser().getUsername());
        lifecycleManager.abortExecution(executionId);
    }

    @Override
    public void suspend(Long executionId) {
        Execution execution = getById(executionId);
        ExecutionLifecycleManager lifecycleManager = managerRepository.findByUsername(execution.getCommand().getUser().getUsername());
        lifecycleManager.suspend(executionId);
    }

    @Override
    public void resume(Long executionId) {
        Execution execution = getById(executionId);
        ExecutionLifecycleManager lifecycleManager = managerRepository.findByUsername(execution.getCommand().getUser().getUsername());
        lifecycleManager.resume(executionId);
    }
}
