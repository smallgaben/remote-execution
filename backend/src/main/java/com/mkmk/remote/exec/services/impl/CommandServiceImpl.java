package com.mkmk.remote.exec.services.impl;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Language;
import com.mkmk.remote.exec.domains.User;
import com.mkmk.remote.exec.repository.CommandRepository;
import com.mkmk.remote.exec.repository.ExecutionLifecycleManagerRepository;
import com.mkmk.remote.exec.repository.UserRepository;
import com.mkmk.remote.exec.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandServiceImpl implements CommandService {

    @Autowired
    private CommandRepository commandRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ExecutionLifecycleManagerRepository executionLifecycleManagerRepository;

    @Override
    public Command create(String username, String name, String description, String language, String code) {
        Command command = new Command();
        command.setName(name);
        command.setDescription(description);
        command.setUser(userRepository.findByUsername(username));
        command.setLang(Language.valueOf(language.trim().toUpperCase()));

        //TODO: add static analyzing of code
        command.setCode(code);

        return commandRepository.save(command);
    }

    @Override
    public Command getById(Long commandId) {
        return commandRepository.findById(commandId);
    }

    @Override
    public List<Command> getAllByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return commandRepository.findByUser(user);
    }
}
