package com.mkmk.remote.exec.repository.impl;

import com.mkmk.remote.exec.domains.Message;
import com.mkmk.remote.exec.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.stream.Collectors;

public class RedisMessageRepository implements MessageRepository {

    @Autowired
    private JedisPool jedisPool;

    @Override
    public void addMessage(Message message) {
        try (Jedis resource = jedisPool.getResource()) {
            resource.zadd("messages:" + message.getExecutionId(), (double) message.getTimestamp(), message.getContent());
        }
    }

    @Override
    public List<Message> getByExecutionId(Long executionId) {
        try (Jedis resource = jedisPool.getResource()) {
            return resource.zrange("messages:" + executionId, 0, -1).stream()
                .map(mess -> new Message(executionId, mess))
                .collect(Collectors.toList());
        }
    }
}
