package com.mkmk.remote.exec.repository;


import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandRepository extends CrudRepository<Command,Long> {

    List<Command> findByUser(User user);

    Command findById(Long commandId);
}
