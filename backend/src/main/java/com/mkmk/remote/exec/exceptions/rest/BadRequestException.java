package com.mkmk.remote.exec.exceptions.rest;

public class BadRequestException extends RestException {
    public BadRequestException(String message) {
        super(message);
    }
}
