package com.mkmk.remote.exec.domains;

public class Message {

    private Long executionId;

    private long timestamp;

    private String content;

    public Message() {
    }

    public Message(Long executionId, String content) {
        this.executionId = executionId;
        this.timestamp = System.currentTimeMillis();
        this.content = content;
    }

    public Long getExecutionId() {
        return executionId;
    }

    public void setExecutionId(Long executionId) {
        this.executionId = executionId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
