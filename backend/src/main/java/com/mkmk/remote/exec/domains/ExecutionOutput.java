package com.mkmk.remote.exec.domains;

import java.util.List;

public class ExecutionOutput {

    private List<Message> output;

    public ExecutionOutput() {
    }

    public ExecutionOutput(List<Message> output) {
        this.output = output;
    }

    public List<Message> getOutput() {
        return output;
    }

    public void setOutput(List<Message> output) {
        this.output = output;
    }
}
