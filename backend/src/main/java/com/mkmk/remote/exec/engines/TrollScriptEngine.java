package com.mkmk.remote.exec.engines;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import static com.mkmk.remote.exec.engines.TrollScriptEngine.Token.NEXT;

public class TrollScriptEngine extends BrainfuckEngine {

    private static final int DEFAULT_TROLL_TOKEN_LENGTH = 3;

    public TrollScriptEngine(int cells) {
        this(cells, new PrintWriter(System.out), System.in);
    }

    public TrollScriptEngine(int cells, Writer out) {
        this(cells, out, System.in);
    }

    public TrollScriptEngine(int cells, Writer out, InputStream in) {
        super(cells, out, in);
    }

    @Override
    public void eval(String str) {
        boolean started = false;
        List<String> tokens = new ArrayList<>();

        while (charPointer < str.length()) {
            String token;
            if (charPointer + DEFAULT_TROLL_TOKEN_LENGTH <= str.length()) {
                token = str.substring(charPointer, charPointer + DEFAULT_TROLL_TOKEN_LENGTH);
            } else {
                token = str.substring(charPointer, charPointer + (str.length() - charPointer));
            }
            if (isValidToken(token)) {
                if (token.equalsIgnoreCase(Token.START)) {
                    started = true;
                } else {
                    if (token.equalsIgnoreCase(Token.END))
                        break;
                    else {
                        if (started) {
                            tokens.add(token);
                        }
                    }
                }
                charPointer += DEFAULT_TROLL_TOKEN_LENGTH;
            } else if (charPointer + DEFAULT_TROLL_TOKEN_LENGTH > str.length()) {
                charPointer += (str.length() - charPointer);
            } else {
                charPointer++;
            }
        }

        int tokenPointer = 0;
        while (tokenPointer < tokens.size()) {
            String token = tokens.get(tokenPointer);

            switch (token.toLowerCase()) {
                case Token.NEXT:
                    dataPointer = (dataPointer == data.length - 1 ? 0 : dataPointer + 1);
                    break;
                case Token.PREVIOUS:
                    dataPointer = (dataPointer == 0 ? data.length - 1 : dataPointer - 1);
                    break;
                case Token.PLUS:
                    data[dataPointer]++;
                    break;
                case Token.MINUS:
                    data[dataPointer]--;
                    break;
                case Token.OUTPUT:
                    try {
                        outWriter.write((char) data[dataPointer]);
                        outWriter.flush();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                    break;
                case Token.INPUT:
                    try {
                        data[dataPointer] = (byte) consoleReader.read();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                    break;
                case Token.BRACKET_LEFT:
                    if (data[dataPointer] == 0) {
                        int level = 1;
                        while (level > 0) {
                            tokenPointer++;
                            if (tokens.get(tokenPointer).equalsIgnoreCase(Token.BRACKET_LEFT)) {
                                level++;
                            } else {
                                if (tokens.get(tokenPointer).equalsIgnoreCase(Token.BRACKET_RIGHT)) {
                                    level--;
                                }
                            }
                        }
                    }
                    break;
                case Token.BRACKET_RIGHT:
                    if (data[dataPointer] != 0) {
                        int level = 1;
                        while (level > 0) {
                            tokenPointer--;
                            if (tokens.get(tokenPointer).equalsIgnoreCase(Token.BRACKET_LEFT)) {
                                level--;
                            } else {
                                if (tokens.get(tokenPointer).equalsIgnoreCase(Token.BRACKET_RIGHT)) {
                                    level++;
                                }
                            }
                        }
                    }
                    break;
            }
            tokenPointer++;
        }
        init(data.length);
    }

    protected boolean isValidToken(String token) {
        return token.equalsIgnoreCase(Token.START)
            || token.equalsIgnoreCase(NEXT)
            || token.equalsIgnoreCase(Token.PREVIOUS)
            || token.equalsIgnoreCase(Token.PLUS)
            || token.equalsIgnoreCase(Token.MINUS)
            || token.equalsIgnoreCase(Token.OUTPUT)
            || token.equalsIgnoreCase(Token.INPUT)
            || token.equalsIgnoreCase(Token.BRACKET_LEFT)
            || token.equalsIgnoreCase(Token.BRACKET_RIGHT)
            || token.equalsIgnoreCase(Token.END);
    }

    protected static final class Token {

        public static final String START = "tro";
        public static final String NEXT = "ooo";
        public static final String PREVIOUS = "ool";
        public static final String PLUS = "olo";
        public static final String MINUS = "oll";
        public static final String OUTPUT = "loo";
        public static final String INPUT = "lol";
        public static final String BRACKET_LEFT = "llo";
        public static final String BRACKET_RIGHT = "lll";
        public static final String END = "ll.";

        private Token() {
        }
    }

}
