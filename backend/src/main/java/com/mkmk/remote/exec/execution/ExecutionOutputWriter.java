package com.mkmk.remote.exec.execution;

import com.mkmk.remote.exec.domains.Message;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;

import java.io.Writer;
import java.util.List;

public class ExecutionOutputWriter extends Writer {

    private Long executionId;

    private List<ExecutionOutputNotifier> notifiers;

    private final StringBuilder builder = new StringBuilder();

    public ExecutionOutputWriter(Long executionId, List<ExecutionOutputNotifier> notifiers) {
        this.executionId = executionId;
        this.notifiers = notifiers;
    }

    @Override
    public void write(char[] buf, int off, int len) {
        if ((off < 0)
            || (off > buf.length)
            || (len < 0)
            || ((off + len) > buf.length)
            || ((off + len) < 0)) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return;
        }
        builder.append(buf, off, len);
        if (buf[buf.length - 1] == '\n'
            || (buf.length > 1 && (buf[buf.length - 2] == '\r' && buf[buf.length - 1] == '\n'))) {
            flush();
        }
    }

    @Override
    public void flush() {
        if (builder.length() > 0) {
            if (builder.charAt(builder.length() - 1) == '\n') {
                builder.setLength(builder.length() - 1);
                if (builder.length() > 0 && builder.charAt(builder.length() - 1) == '\r') {
                    builder.setLength(builder.length() - 1);
                }
            }

            notifiers.forEach(nt -> nt.notifyOutput(new Message(executionId, builder.toString())));

            builder.setLength(0);
        }
    }

    @Override
    public void close() {
        flush();
    }
}
