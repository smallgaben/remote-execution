package com.mkmk.remote.exec.engines;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class OokEngine extends TrollScriptEngine {

    private static final int DEFAULT_OOK_TOKEN_LENGTH = 9;

    public OokEngine(int cells) {
        this(cells, new PrintWriter(System.out), System.in);
    }

    public OokEngine(int cells, Writer out) {
        this(cells, out, System.in);
    }

    public OokEngine(int cells, Writer out, InputStream in) {
        super(cells, out, in);
    }

    @Override
    public void eval(String str) {
        List<Token> tokens = new ArrayList<>();
        while (charPointer < str.length()) {
            String token;
            if (charPointer + DEFAULT_OOK_TOKEN_LENGTH <= str.length()) {
                token = str.substring(charPointer, charPointer + DEFAULT_OOK_TOKEN_LENGTH);
            } else {
                token = str.substring(charPointer, charPointer + (str.length() - charPointer));
            }
            boolean b = false;

            for (Token tokenCheck : Token.values()) {
                if (tokenCheck.getValue().equals(token)) {
                    tokens.add(tokenCheck);
                    charPointer += DEFAULT_OOK_TOKEN_LENGTH;
                    b = true;
                    break;
                }
            }

            if (!b)
                if (charPointer + DEFAULT_OOK_TOKEN_LENGTH > str.length()) {
                    charPointer += (str.length() - charPointer);
                } else {
                    charPointer++;
                }
        }

        int tokenPointer = 0;
        while (tokenPointer < tokens.size()) {
            Token token = tokens.get(tokenPointer);
            switch (token) {
                case NEXT:
                    dataPointer = (dataPointer == data.length - 1 ? 0 : dataPointer + 1);
                    break;
                case PREVIOUS:
                    dataPointer = (dataPointer == 0 ? data.length - 1 : dataPointer - 1);
                    break;
                case PLUS:
                    data[dataPointer]++;
                    break;
                case MINUS:
                    data[dataPointer]--;
                    break;
                case OUTPUT:
                    try {
                        outWriter.write(data[dataPointer]);
                        outWriter.flush();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                    break;
                case INPUT:
                    try {
                        data[dataPointer] = (byte) consoleReader.read();
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                    break;
                case BRACKET_LEFT:
                    if (data[dataPointer] == 0) {
                        int level = 1;
                        while (level > 0) {
                            tokenPointer++;
                            if (tokens.get(tokenPointer).equals(Token.BRACKET_LEFT)) {
                                level++;
                            } else {
                                if (tokens.get(tokenPointer).equals(Token.BRACKET_RIGHT)) {
                                    level--;
                                }
                            }
                        }
                    }
                    break;
                case BRACKET_RIGHT:
                    if (data[dataPointer] != 0) {
                        int level = 1;
                        while (level > 0) {
                            tokenPointer--;
                            if (tokens.get(tokenPointer).equals(Token.BRACKET_LEFT)) {
                                level--;
                            } else {
                                if (tokens.get(tokenPointer).equals(Token.BRACKET_RIGHT)) {
                                    level++;
                                }
                            }
                        }
                    }
                    break;
            }
            tokenPointer++;
        }
        init(data.length);
    }

    protected enum Token {

        NEXT("Ook. Ook?"),
        PREVIOUS("Ook? Ook."),
        PLUS("Ook. Ook."),
        MINUS("Ook! Ook!"),
        OUTPUT("Ook! Ook."),
        INPUT("Ook. Ook!"),
        BRACKET_LEFT("Ook! Ook?"),
        BRACKET_RIGHT("Ook? Ook!");

        String value;

        Token(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
