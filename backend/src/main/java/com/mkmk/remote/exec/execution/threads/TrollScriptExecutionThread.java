package com.mkmk.remote.exec.execution.threads;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.engines.TrollScriptEngine;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;

import java.io.Writer;
import java.util.List;

public class TrollScriptExecutionThread extends ExecutionThread {

    private static final int MEMORY_CELLS = 100000;

    public TrollScriptExecutionThread(Execution execution, List<ExecutionOutputNotifier> notifiers) {
        super(execution, notifiers);
    }

    @Override
    protected void evaluate(Writer writer) {
        TrollScriptEngine engine = new TrollScriptEngine(MEMORY_CELLS, writer);
        engine.eval(getExecution().getCommand().getCode());
    }
}
