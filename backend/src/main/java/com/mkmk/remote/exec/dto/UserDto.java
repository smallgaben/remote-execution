package com.mkmk.remote.exec.dto;

import com.mkmk.remote.exec.domains.User;

public class UserDto {

    private String username;
    private String password;
    private String email;

    public UserDto() {
    }

    public UserDto(User other) {
        this.username = other.getUsername();
        this.email = other.getPassword();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserDto{" +
            "username='" + username + '\'' +
            ", email='" + email + '\'' +
            '}';
    }
}
