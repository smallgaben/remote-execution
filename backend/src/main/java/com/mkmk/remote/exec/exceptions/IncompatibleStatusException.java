package com.mkmk.remote.exec.exceptions;

public class IncompatibleStatusException extends RuntimeException {
    public IncompatibleStatusException(String message) {
        super(message);
    }
}
