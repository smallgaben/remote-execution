package com.mkmk.remote.exec.execution.threads;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.engines.BrainfuckEngine;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;

import java.io.Writer;
import java.util.List;

public class BrainfuckExecutionThread extends ExecutionThread {

    private static final int MEMORY_CELLS = 100000;

    public BrainfuckExecutionThread(Execution execution, List<ExecutionOutputNotifier> notifiers) {
        super(execution, notifiers);
    }

    @Override
    protected void evaluate(Writer writer) {
        BrainfuckEngine engine = new BrainfuckEngine(MEMORY_CELLS, writer);
        engine.eval(getExecution().getCommand().getCode());
    }
}
