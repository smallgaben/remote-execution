package com.mkmk.remote.exec.execution;

import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.domains.Status;
import com.mkmk.remote.exec.exceptions.IncompatibleStatusException;
import com.mkmk.remote.exec.exceptions.ParallelExecutionOverflowException;
import com.mkmk.remote.exec.execution.threads.ExecutionThread;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//TODO: maybe another naming would be better
public class ExecutionLifecycleManager {

    private final int parallelExecutionsLimit;
    private final ExecutionThreadsHolder executionThreadsHolder;
    private final Map<Long, ExecutionThread> executionThreadMap = new ConcurrentHashMap<>();

    public ExecutionLifecycleManager(ExecutionThreadsHolder executionThreadsHolder, int parallelExecutionsLimit) {
        this.executionThreadsHolder = executionThreadsHolder;
        this.parallelExecutionsLimit = parallelExecutionsLimit;
    }

    public void start(Execution execution) {
        if (isParallelExecutionsLimitReached()) {
            throw new ParallelExecutionOverflowException(
                String.format("Parallel executions limit reached for user [%s]", execution.getCommand().getUser())
            );
        }

        ExecutionThread executionThread = executionThreadsHolder.getExecutionThread(execution);
        executionThread.start();
        executionThreadMap.put(execution.getId(), executionThread);
    }

    public void abortExecution(Long executionId) {
        ExecutionThread executionThread = executionThreadMap.get(executionId);
        checkExecutionThreadStatus(executionThread, Status.EXECUTING);
        executionThread.stop();
        executionThread.getExecution().setStatus(Status.ABORTED);
    }

    public void suspend(Long executionId) {
        ExecutionThread executionThread = executionThreadMap.get(executionId);
        checkExecutionThreadStatus(executionThread, Status.EXECUTING);
        executionThread.suspend();
        executionThread.getExecution().setStatus(Status.PAUSED);
    }

    public void resume(Long executionId) {
        ExecutionThread executionThread = executionThreadMap.get(executionId);
        checkExecutionThreadStatus(executionThread, Status.PAUSED);
        executionThread.resume();
        executionThread.getExecution().setStatus(Status.EXECUTING);
    }

    private void checkExecutionThreadStatus(ExecutionThread executionThread, Status expectedStatus) {
        Status currentStatus = executionThread.getExecution().getStatus();
        if (currentStatus != expectedStatus) {
            throw new IncompatibleStatusException(String.format("ExecutionThread [%d] has status [%s], but expected [%s]"
                , executionThread.getId(), currentStatus, expectedStatus));
        }
    }

    private boolean isParallelExecutionsLimitReached() {
        long count = executionThreadMap.values().stream()
            .filter(it -> Status.EXECUTING == it.getExecution().getStatus())
            .count();

        return count >= parallelExecutionsLimit;
    }
}
