package com.mkmk.remote.exec.repository.impl.inmemory;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.repository.ExecutionRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class InMemoryExecutionRepository implements ExecutionRepository {

    private Map<Long, Execution> executionMap = new ConcurrentHashMap<>();
    private AtomicLong counter = new AtomicLong();

    @Override
    public Execution create(Command command) {
        Execution execution = new Execution();
        execution.setId(counter.incrementAndGet());
        execution.setCommand(command);
        executionMap.put(execution.getId(), execution);

        return execution;
    }

    @Override
    public Execution getById(Long executionId) {
        return executionMap.get(executionId);
    }

    @Override
    public List<Execution> getAllByUsername(String username) {
        return executionMap.entrySet().stream()
            .filter(es -> es.getValue().getCommand().getUser().getUsername().equals(username))
            .map(Map.Entry::getValue)
            .collect(Collectors.toList());
    }
}
