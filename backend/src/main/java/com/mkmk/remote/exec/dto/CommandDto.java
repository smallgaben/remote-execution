package com.mkmk.remote.exec.dto;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Status;
import org.hibernate.validator.constraints.NotBlank;

public class CommandDto {

    private Long id;
    @NotBlank(message = "Command's name can't be empty")
    private String name;
    private String description;
    private String username;
    private String lang;
    private String code;

    public CommandDto() {
    }

    public CommandDto(Command command) {
        this.id = command.getId();
        this.name = command.getName();
        this.description = command.getDescription();
        this.username = command.getUser().getUsername();
        this.lang = command.getLang().toString();
        this.code = command.getCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "CommandDto{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", username='" + username + '\'' +
            ", lang='" + lang + '\'' +
            ", code='" + code + '\'' +
            '}';
    }
}
