package com.mkmk.remote.exec.execution.threads;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.exceptions.ExecutionException;
import com.mkmk.remote.exec.execution.notifiers.ExecutionOutputNotifier;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.Writer;
import java.util.List;

public class PythonExecutionThread extends ExecutionThread {

    private final ScriptEngine engine;

    public PythonExecutionThread(Execution execution, List<ExecutionOutputNotifier> notifiers, ScriptEngine engine) {
        super(execution, notifiers);
        this.engine = engine;
    }

    @Override
    protected void evaluate(Writer writer) {
        engine.getContext().setWriter(writer);
        engine.getContext().setErrorWriter(writer);
        try {
            engine.eval(getExecution().getCommand().getCode());
        } catch (ScriptException e) {
            throw new ExecutionException(e);
        }
    }
}
