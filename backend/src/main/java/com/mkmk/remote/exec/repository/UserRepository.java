package com.mkmk.remote.exec.repository;

import com.mkmk.remote.exec.domains.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

    User findById(Long id);

    List<User> findByUsernameOrEmail(String username, String email);
}
