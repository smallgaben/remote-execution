package com.mkmk.remote.exec.engines;

import com.mkmk.remote.exec.exceptions.ExecutionException;

import java.io.File;

public interface CustomScriptEngine {

    void eval(String code) throws ExecutionException;

    void eval(File source) throws ExecutionException;
}
