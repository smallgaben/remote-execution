package com.mkmk.remote.exec.dto;

public class ResourceIdDto {
    private Long id;

    public ResourceIdDto() {
    }

    public ResourceIdDto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
