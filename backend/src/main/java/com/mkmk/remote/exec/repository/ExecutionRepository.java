package com.mkmk.remote.exec.repository;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;

import java.util.List;

public interface ExecutionRepository {

    Execution create(Command command);

    Execution getById(Long executionId);

    List<Execution> getAllByUsername(String username);
}
