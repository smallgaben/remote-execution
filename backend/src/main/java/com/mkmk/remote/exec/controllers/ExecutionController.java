package com.mkmk.remote.exec.controllers;

import com.mkmk.remote.exec.domains.Command;
import com.mkmk.remote.exec.domains.Execution;
import com.mkmk.remote.exec.dto.ResourceIdDto;
import com.mkmk.remote.exec.exceptions.ResourceNotFoundException;
import com.mkmk.remote.exec.exceptions.rest.BadRequestException;
import com.mkmk.remote.exec.services.CommandService;
import com.mkmk.remote.exec.services.ExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
public class ExecutionController extends AbstractController {

    @Autowired
    private CommandService commandService;
    @Autowired
    private ExecutionService executionService;

    @PostMapping("/executions")
    public ResponseEntity executeCommand(@RequestBody ResourceIdDto dto) {
        Command command;
        try {
            command = commandService.getById(dto.getId());
        } catch (ResourceNotFoundException e) {
            throw new BadRequestException(String.format("Command with id [%s] not found", dto.getId()));
        }

        Execution execution;
        try {
            execution = executionService.create(command);
        } catch (Exception e) {
            //TODO: executionService exceptions
            throw new RuntimeException();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(execution.getId());
    }

    @GetMapping("/executions")
    public ResponseEntity<List<Execution>> getExecutions(Principal principal) {
        return ResponseEntity.ok(executionService.getAllByUsername(principal.getName()));
    }

    @GetMapping("/executions/{executionId}")
    public ResponseEntity<Execution> getExecution(@PathVariable Long executionId) {
        try {
            return ResponseEntity.ok(executionService.getById(executionId));
        } catch (ResourceNotFoundException e) {
            throw new BadRequestException(String.format("Execution with id [%s] not found", executionId));
        }
    }

    //TODO: fix in a REST way with PUT method, handle not found exceptions

    @PostMapping("/executions/{executionId}/suspend")
    public ResponseEntity<Execution> suspendExecution(@PathVariable Long executionId) {
        executionService.suspend(executionId);
        return ResponseEntity.status(HttpStatus.OK).body(executionService.getById(executionId));
    }

    @GetMapping("/executions/{executionId}/resume")
    public ResponseEntity<Execution> resumeExecution(@PathVariable Long executionId) {
        executionService.resume(executionId);
        return ResponseEntity.status(HttpStatus.OK).body(executionService.getById(executionId));
    }

    @GetMapping("/executions/{executionId}/abortExecution")
    public ResponseEntity<Execution> abortExecution(@PathVariable Long executionId) {
        executionService.abort(executionId);
        return ResponseEntity.status(HttpStatus.OK).body(executionService.getById(executionId));
    }
}
