package com.mkmk.remote.exec.repository.impl.inmemory;

import com.mkmk.remote.exec.execution.ExecutionThreadsHolder;
import com.mkmk.remote.exec.execution.ExecutionLifecycleManager;
import com.mkmk.remote.exec.repository.ExecutionLifecycleManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class ExecutionLifecycleManagerRepositoryImpl implements ExecutionLifecycleManagerRepository {

    private static final int DEFAULT_RUNNABLE_COMMANDS_SIZE = 5;
    private Map<String, ExecutionLifecycleManager> userToCommandRunnerMap = new ConcurrentHashMap<>();

    @Autowired
    private ExecutionThreadsHolder executionThreadsHolder;

    @Override
    public ExecutionLifecycleManager create(String username) {
        ExecutionLifecycleManager runner = new ExecutionLifecycleManager(executionThreadsHolder, DEFAULT_RUNNABLE_COMMANDS_SIZE);
        userToCommandRunnerMap.put(username, runner);
        return runner;
    }

    @Override
    public ExecutionLifecycleManager findByUsername(String username) {
        if(!userToCommandRunnerMap.containsKey(username)) {
            return create(username);
        }
        return userToCommandRunnerMap.get(username);
    }
}
