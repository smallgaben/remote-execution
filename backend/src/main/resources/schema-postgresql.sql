CREATE TABLE IF NOT EXISTS users
(
  id       BIGSERIAL   NOT NULL
    CONSTRAINT users_pkey
    PRIMARY KEY,
  username VARCHAR(24) NOT NULL,
  password VARCHAR(64) NOT NULL,
  email    VARCHAR(32) NOT NULL
);

CREATE TABLE IF NOT EXISTS public.commands
(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(250),
  description VARCHAR(1000),
  code VARCHAR,
  user_id BIGINT NOT NULL,
  language INT
);
